import { Component, ViewChildren } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import CommonConstants from 'src/app/shared/constants/global.const';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader/loader.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { TokenService } from 'src/app/shared/services/token.service';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.scss']
})
export class OtpComponent {
  otpForm: FormGroup;
  otpInput = ["input1", "input2", "input3", "input4"];
  @ViewChildren("formRow") rows: any;
  qParams: any;
  formBuilder: any;
  @ViewChildren('firstInput') vc: any;
  @ViewChildren('getOtpButton') vc1: any;

  constructor(
    // private toasterService: ToastService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private httpService: HttpService,
    private loaderService: LoaderService,
    private tokenService: TokenService,
    private storageService:StorageService,
    private toastr: ToastrService,
  ){
    this.otpForm = this.toFormGroup(this.otpInput);
    this.qParams = this.router.getCurrentNavigation()?.extras.state?.["queryParams"];
    console.log('this.qParams', this.qParams)
  }
  // toFormGroup(otpInput: string[]): FormGroup<any> {
  //   throw new Error('Method not implemented.');
  // }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.vc.first.nativeElement.focus();
  }

  checkIfFormValid() {
    setTimeout(() => {
      if (this.otpForm.valid) {
        this.vc1.first.nativeElement.focus();
      }
    }, 1000);
  }

  ionViewDidEnter() { }

  continue() {

    let endPoint;
    let reqBody;
    if (this.qParams.comingFrom == "signUp") {
      endPoint = "registerOtpVerify";
      reqBody = this.qParams.mobile;
    } else {
      endPoint = "otpVerify";
      reqBody = '91' + this.qParams.mobile;
    }

    let sendData = {
      value: reqBody,
      otp: Object.values(this.otpForm.value).join(""),
      // deviceRegistrationToken: this.storageService.getItem(CommonConstants.PUSH_TOKEN)
    };
    this.httpService.post(endPoint, sendData).subscribe({
      next: (response: any) => {
        this.router.navigate(["/dashboard/default"]);
        this.tokenService.setToken(response.token);
        this.storageService.setItem(CommonConstants.USER_DETAILS_KEY, JSON.stringify(response));
        this.authenticationService.isAuthenticated.next(true);
        this.toastr.success('Login successfully..!');
      },
      error: (err: any) => {
        console.log("err", err);
        this.toastr.error(err?.error?.messages.error);
        // const errorMessage = err?.messages?.error;
        // if (errorMessage) {
        //   this.toastr.error(errorMessage);
        // } else {
        //   this.toastr.error("An error occurred."); // Fallback message
        // }
        
     
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  keyUpEvent(event: any, index: any) {
    let pos = index;
    if (event.keyCode === 8 && event.which === 8) {
      pos = index - 1;
    } else {
      pos = index + 1;
    }
    if (pos > -1 && pos < this.otpInput.length) {
      this.rows._results[pos].nativeElement.focus();
    }
  }

  toFormGroup(elements: any) {
    const group: any = {};
    elements.forEach((key: any) => {
      group[key] = new FormControl("", Validators.required);
    });
    return new FormGroup(group);
  }

  verified() {
    
    this.authenticationService.login(this.qParams.mobile).subscribe({
      next: (response) => {

      },
      error: (err) => {
        console.log("err", err);
   
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
    
  }
  // continue() {
  //   let sendData = {
  //     value: '91' +this.qParams.mobile,
  //     otp: this.otpForm.get('otp')?.value,
  //   };
  //   this.httpService.post('otpVerify', sendData).subscribe({
  //     next: (response: any) => {
  //       this.tokenService.setToken(response.token);
  //       this.storageService.setItem(CommonConstants.USER_DETAILS_KEY, JSON.stringify(response));
  //       this.authenticationService.isAuthenticated.next(true);
  //       this.router.navigate(["/dashboard/default"]);
  //       // setTimeout(() => {
  //       //   this.loaderService.stopLoader();
  //       // }, 200);
  //     },
  //     error: (err: any) => {
  //       console.log("err", err);
  //       // setTimeout(() => {
  //       //   this.loaderService.stopLoader();
  //       // }, 200);
  //     },
  //     complete: () => {
  //       console.info("complete.!!");
  //     },
  //   });
  // }

  // keyUpEvent(event: any, index: any) {
  //   let pos = index;
  //   if (event.keyCode === 8 && event.which === 8) {
  //     pos = index - 1;
  //   } else {
  //     pos = index + 1;
  //   }
  //   if (pos > -1 && pos < this.otpInput.length) {
  //     this.rows._results[pos].nativeElement.focus();
  //   }
  // }

  // toFormGroup(elements: any) {
  //   const group: any = {};
  //   elements.forEach((key: any) => {
  //     group[key] = new FormControl("", Validators.required);
  //   });
  //   return new FormGroup(group);
  // }




//   verified() {

//     this.authenticationService.login(this.qParams.mobile).subscribe({
//       next: (response) => {
      
//         // this.toasterService.success("OTP send successfully..!")
//       },
//       error: (err) => {
//         console.log("err", err);
      
//         // this.toasterService.danger(err?.error?.messages.error);
//       },
//       complete: () => {
//         console.info("complete.!!");
//       },
//     });
    
// }
}
