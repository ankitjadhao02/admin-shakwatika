import { Component, OnInit, ViewChildren } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NavigationExtras, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { AuthenticationService } from "src/app/shared/services/authentication.service";

@Component({
    selector: "app-login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
    public show: boolean = false;
    public loginForm: FormGroup;
    public errorMessage: any;
  @ViewChildren('firstInput') vc: any;
  @ViewChildren('getOtpButton') vc1: any;



    constructor(
        private fb: FormBuilder,
        public router: Router,
        private authenticationService: AuthenticationService,
        private toastr: ToastrService,
        // private toasterService: ToastService,
        // private loaderService: LoaderService,
        ) {
        this.loginForm = this.fb.group({
            mobileNo: ["", [Validators.required, Validators.pattern('^[0-9]{10}$')]],
            // email: ["Test@gmail.com", [Validators.required, Validators.email]],
            // password: ["test123", Validators.required],
        });
    }

    ngOnInit() { }

    ngAfterViewInit() {
      this.vc.first.nativeElement.focus();
    }

    checkIfFormValid() {
      setTimeout(() => {
        if (this.loginForm.valid) {
          this.vc1.first.nativeElement.focus();
        }
      }, 1000);
    }

    showPassword() {
        this.show = !this.show;
    }

    login(): void {
        this.authenticationService.login(this.loginForm.value.mobileNo).subscribe({
          next: (response) => {
            let objToSend: NavigationExtras = {
              queryParams: { mobile: this.loginForm.value.mobileNo, comingFrom: 'signIn' },
              skipLocationChange: false,
              fragment: 'top'
            };
            this.router.navigate(['auth/otpVerify'], { state: objToSend });
            // this.toastr.success(response?.messages?.error);
            // setTimeout(() => {
            //   this.loaderService.stopLoader();
            // }, 200);
            // this.toasterService.success("OTP send successfully..!");
            /**
             * This line is commented as there is no actual OTP is implemented
             * Remove skipOtpScreen function and Un-comment router.navigate
             */
            // this.authenticationService.skipOtpScreen("", this.loginForm.value.mobileNo);
          },
          error: (err) => {
            console.log("err", err);
            this.toastr.error(err?.error?.messages.error); // Fallback message

            // const errorMessage = err?.messages?.error;
            // if (errorMessage) {
            //   this.toastr.error(errorMessage);
            // } else {
            //   this.toastr.error("An error occurred."); // Fallback message
            // }
          },
          complete: () => {
            console.info("complete.!!");
          },
        });
      }

    // Simple Login
    // login() {
    //     if (this.loginForm.value["email"] == "Test@gmail.com" && this.loginForm.value["password"] == "test123") {
    //         let user = {
    //             email: "Test@gmail.com",
    //             password: "test123",
    //             name: "test user",
    //         };
    //         localStorage.setItem("user", JSON.stringify(user));
    //         this.router.navigate(["/dashboard/default"]);
    //     }
    // }


}