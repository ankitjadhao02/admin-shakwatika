export default class CommonConstants {
  public static APP_VERSION = '1.0.0';

  public static HIDE_TAB_PAGES: Array<string> = [""];
  public static HIDE_LOCATION_BAR_PAGES: Array<string> = [""];

  // Storage Keys
  public static INTRO_KEY = 'introSeen_curiotory';
  public static TOKEN_KEY = "accessToken_curiotory";
  public static LANGUAGE_KEY = "selectedLanguage_curiotory";
  public static USER_DETAILS_KEY = "userDetails_curiotory";
  public static PUSH_TOKEN = 'pushToken_curiotory';

  // Variables Key
  public static ALPHABETS = "abcdefghijklmnopqrstuvwxyz";

  public static QA_Resources: any = [
    {
      languageId: "1",
      href: "https://curiotory-my.sharepoint.com/personal/kalyani_curiotory_com/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fkalyani%5Fcuriotory%5Fcom%2FDocuments%2FLanguage%20Q%26A%2FMandarin%20Q%20%26A&ga=1"
    },
    {
      languageId: "2",
      href: "https://curiotory-my.sharepoint.com/personal/kalyani_curiotory_com/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fkalyani%5Fcuriotory%5Fcom%2FDocuments%2FLanguage%20Q%26A%2FGerman%20Q%20%26%20A&ga=1"
    },
    {
      languageId: "3",
      href: "https://curiotory-my.sharepoint.com/personal/kalyani_curiotory_com/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fkalyani%5Fcuriotory%5Fcom%2FDocuments%2FLanguage%20Q%26A%2FFrench%20Q%26%20A&ga=1"
    },
    {
      languageId: "4",
      href: "https://curiotory-my.sharepoint.com/personal/kalyani_curiotory_com/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fkalyani%5Fcuriotory%5Fcom%2FDocuments%2FLanguage%20Q%26A%2FSpanish%20Q%20%26%20A&ga=1"
    },

  ];
}

export class AppConfigConstants {
  public static APP_CONFIG: any = {
    menuType: "sidemenu_tabs" /** sidemenu | tabs | sidemenu_tabs */,
  };
}

export class RouteConstants {
  public static SIDE_MENU: any = [
    {
      title: "Subscription Plans",
      icon: "card-outline",
      image: "",
      urlPath: "",
      path: "",
      parent: "",
      isSidemenu: true,
      isTab: false,
    },
    {
      title: "Learn More",
      icon: "reader-outline",
      image: "",
      urlPath: "",
      path: "",
      parent: "",
      isSidemenu: true,
      isTab: false,
    },
    {
      title: "Contact Us",
      icon: "call-outline",
      image: "",
      urlPath: "",
      path: "",
      parent: "",
      isSidemenu: true,
      isTab: false,
    },
    {
      title: "About Us",
      icon: "information-circle-outline",
      image: "",
      urlPath: "",
      path: "",
      parent: "",
      isSidemenu: true,
      isTab: false,
    },
    {
      title: "Terms & Condition",
      icon: "calendar-clear-outline",
      image: "",
      urlPath: "",
      path: "",
      parent: "",
      isSidemenu: true,
      isTab: false,
    },
    {
      title: "FAQ",
      icon: "chatbox-ellipses-outline",
      image: "",
      urlPath: "",
      path: "",
      parent: "",
      isSidemenu: true,
      isTab: false,
    },
    {
      title: "Rate Us",
      icon: "star-outline",
      image: "",
      urlPath: "",
      path: "",
      parent: "",
      isSidemenu: true,
      isTab: false,
    },
    {
      title: "Share",
      icon: "paper-plane-outline",
      image: "",
      urlPath: "",
      path: "",
      parent: "",
      isSidemenu: true,
      isTab: false,
    },
    {
      title: "Ask The Expert",
      icon: "chatbubbles-outline",
      image: "",
      urlPath: "",
      path: "",
      parent: "",
      isSidemenu: true,
      isTab: false,
    },
  ];

  public static ROUTE_REGISTRY: any = [
    {
      title: "Orders",
      icon: "bi bi-cart-check",
      image: "",
      urlPath: "/pages/orders",
      path: "orders",
      parent: "App",
      isMenuTab: true,
      isPartOfRouting: true,
      children: [],
    },
  ];
}

