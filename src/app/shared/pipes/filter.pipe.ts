import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    console.log('FilterPipe invoked:', items, searchText);
    // if (!items) {
    //   return [];
    // }
    // if (!searchText) {
    //   return items;
    // }
    searchText = searchText.toLowerCase();
    return items.filter(item => item.name.toLowerCase().includes(searchText));
  }
}
