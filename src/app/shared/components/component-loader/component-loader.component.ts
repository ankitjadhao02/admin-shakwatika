import { Component } from '@angular/core';
import { LoaderService } from '../../services/loader/loader.service';

@Component({
  selector: 'app-component-loader',
  templateUrl: './component-loader.component.html',
  styleUrls: ['./component-loader.component.scss']
})
export class ComponentLoaderComponent {
  show: boolean;

  constructor(private loaderService: LoaderService) {
    this.loaderService.loaderVisibilitySubscription.subscribe((res: any) => {
      this.show = res;
    })
  }
}
