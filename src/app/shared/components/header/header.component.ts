import { Component, OnInit } from '@angular/core';
import { NavService } from 'src/app/shared/services/nav/nav.service';
import { AuthenticationService } from '../../services/authentication.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  collapseSidebar: boolean = true;
  constructor(private navServices: NavService, private authenticationService : AuthenticationService) { }
  open = false;

  sidebarToggle() {
    this.navServices.collapseSidebar = !this.navServices.collapseSidebar;
  }
  logoutFunc() {
    // this.authService.SignOut();
  }
  ngOnInit(): void { 
  }
  // menu open
  openMenu(){
    this.open = !this.open
  }

  languageToggle() {
    this.navServices.language = !this.navServices.language;
  }
  // async logout() {
  //   this.authenticationService.logout();
  //   // let confirmed: any = await this.alertService.showConfirmation("Logout!", "Are you sure want to Logout?", "No", "Yes");
  //   // if (confirmed) {
  //   //   this.loaderService.startLoader();
  //   //   setTimeout(() => {
  //   //     this.loaderService.stopLoader();
  //   //   }, 200);
  //   // }
  // }

  async logout() {
    const result = await Swal.fire({
      title: 'Logout',
      text: "Are you sure?",
      // icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, log out'
    });
  
    if (result.isConfirmed) {
      // Perform the logout action here
      this.authenticationService.logout();
    }
  }

}
