import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { BehaviorSubject, Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { HttpService } from "./http/http.service";
// import { LoaderService } from "./loader/loader.service";
import { StorageService } from "./storage.service";
import { TokenService } from "./token.service";
import CommonConstants from "../constants/global.const";
// import { HttpService } from "./http.service";
// import CommonConstants from "../constants/global.const";
// import { StorageService } from "./storage.service";
// import { LoaderService } from "./loader.service";
// import { TokenService } from "./token.service";
// import { ToastService } from "./toast.service";


@Injectable({
  providedIn: "root",
})
export class AuthenticationService {
  token = this.storageService.getItem(CommonConstants.TOKEN_KEY);
  isAuthenticated: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(
    private http: HttpClient, 
    private httpService: HttpService,
    private storageService:StorageService,
    private tokenService: TokenService,
    // private loaderService: LoaderService,
    // private toasterService: ToastService,
    private router: Router
    ) {
      this.loadToken();
    }

  async loadToken() {
    if (this.token) {
      this.isAuthenticated.next(true);
    } else {
      this.isAuthenticated.next(false);
    }
  }

  login(mobileNo: any, password?:any, ): Observable<any> {
    let dataTosend = {
      option: "mobileNo",
      value: '91' + mobileNo,
      // email:email,
      // password: password,
      // deviceRegistrationToken: this.storageService.getItem(CommonConstants.PUSH_TOKEN) || '1234'
    };
    return this.httpService.post("login", dataTosend);
  }

  skipOtpScreen(comingFrom:any,mobileNo:any){
    // this.loaderService.startLoader();
    let endPoint;
    let reqBody;
    if (comingFrom == "signUp") {
      endPoint = "registerOtpVerify";
      reqBody = mobileNo;
    } else {
      endPoint = "otpVerify";
      reqBody = '91' + mobileNo;
    }

    let sendData = {
      value: reqBody,
      otp: "1234",
      // deviceRegistrationToken: this.storageService.getItem(CommonConstants.PUSH_TOKEN)
    };

    this.httpService.post(endPoint, sendData).subscribe({
      next: (response: any) => {
        this.tokenService.setToken(response.token);
        this.storageService.setItem(CommonConstants.USER_DETAILS_KEY, JSON.stringify(response));
        this.isAuthenticated.next(true);
        // setTimeout(() => {
        //   this.loaderService.stopLoader();
        // }, 200);
        // this.toasterService.success("Login successfully..!");
        this.router.navigate(["/dashboard/default"]);
      },
      error: (err: any) => {
        console.log("err", err);
        // setTimeout(() => {
        //   this.loaderService.stopLoader();
        // }, 200);
        // this.toasterService.danger(err?.error?.messages.error);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  proceessLoginWithEmailPassword(response:any){
    this.tokenService.setToken(response.token);
    this.storageService.setItem(CommonConstants.USER_DETAILS_KEY, JSON.stringify(response));
    this.isAuthenticated.next(true);
    this.router.navigate(["/dashboard/default"]);
  }

  logout() {
    this.httpService.get("logOut").subscribe({
      next: (response) => {
        this.isAuthenticated.next(false)
        this.storageService.removeItem(CommonConstants.USER_DETAILS_KEY);
        this.storageService.removeItem(CommonConstants.LANGUAGE_KEY);
        this.storageService.removeItem(CommonConstants.TOKEN_KEY);
        // this.storageService.removeItem(CommonConstants.PUSH_TOKEN);
        this.reloadApp();
      },
      error: (err) => {
        console.log("err", err);
      },
      complete: () => {
        console.info("complete.!!");
      },
    });
  }

  reloadApp(): void {
    window.location.reload();
  }

}
