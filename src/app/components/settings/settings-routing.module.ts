import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UnlockingUserComponent } from './unlocking-user/unlocking-user.component';

var routingAnimation = localStorage.getItem('animate')

const routes: Routes = [
  {
    path: 'unlock',
    component: UnlockingUserComponent,
    data: {
      title: 'Unlocking User',
      breadcrumb: 'Unlocking User',
      animation: [routingAnimation]
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
