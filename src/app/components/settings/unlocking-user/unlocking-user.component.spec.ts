import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnlockingUserComponent } from './unlocking-user.component';

describe('UnlockingUserComponent', () => {
  let component: UnlockingUserComponent;
  let fixture: ComponentFixture<UnlockingUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnlockingUserComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UnlockingUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
