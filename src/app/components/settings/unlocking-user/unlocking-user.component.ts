import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from 'src/app/shared/services/http/http.service';

@Component({
  selector: 'app-unlocking-user',
  templateUrl: './unlocking-user.component.html',
  styleUrls: ['./unlocking-user.component.scss']
})
export class UnlockingUserComponent {
  public validate = false;
  public tooltipValidation = false;
  unlockForm: FormGroup;
  userData: any;


  constructor(private httpService: HttpService, private fb: FormBuilder) {
    this.formUnlock();
    this.onChangeUserSelect();
  }

  ngOnInit(): void {
    this.getUserList();
  }

  formUnlock() {
    this.unlockForm = this.fb.group({
      user: [null, Validators.required],
      status: ['', Validators.required],
    });
  }

  onChangeUserSelect() {
    this.unlockForm.controls['user'].valueChanges.subscribe(change => {
      if (change) {
        this.unlockForm.get('status')?.setValue(change.status);
      }
    });
  }


  getUserList() {
    this.httpService.get('users/getUsersList').subscribe(
      (res) => {
        console.log('res', res)
        this.userData = res.data;
      },

      (error) => {
        console.log('error', error)
      }
    )
  }

  updateExpertProfileList() {
    console.log('this.unlockForm.value.user', this.unlockForm.value.user)
    let sendData = {
      status: this.unlockForm?.value.status
    }

    this.httpService.put('users/updateUsers', this.unlockForm.value.user._id, sendData).subscribe(
      (res) => {
        console.log('res', res)
        this.getUserList();
        this.unlockForm.reset();
      },

      (error) => {
        console.log('error', error)
      }
    )
  }

  deleteUserList(i: any) {
    this.httpService.delete('users/deleteUsersList', i).subscribe(
      (res) => {
        console.log('res', res)
        this.getUserList();
      },

      (error) => {
        console.log('error', error)
      }
    )
  }
}
