import { Component, HostListener, QueryList, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { ProductListDirective } from 'src/app/shared/directives/product-list.directive';
import { SortEvent } from 'src/app/shared/directives/sortable.directive';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader/loader.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent {
  headers!: QueryList<ProductListDirective>;
  searchText: any = "";
  categoryForm: FormGroup;
  pagination = {
    limit: 5,
    currentPage: 1,
    totalCount: 0,
    hasNextPage: false,
    hasPrevPage: false,
  }
  selectedId: any;
  roleDetails: any;
  categoryDetails: any=[];
  singleCategoryId: any;
  qParams: any;
  userId: string | null;
  reciveduser: any;
  modalRef?: BsModalRef;
  pagedCategoryDetails: any[] = [];
  isEditing: boolean = false;
  currentPage : any = 1;
  itemsPerPage = 5;
 
  constructor(
    private httpService: HttpService,
    private loaderService: LoaderService,
    private router: Router,
    private modalService: BsModalService,
    private fb: FormBuilder, 
    private toastr: ToastrService,
    ) {
    /*   this.qParams = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
      console.log('this.qParams', this.qParams) */
      this.expertForm();
  }

  ngOnInit(): void {
    this.userId = this.httpService.getUserId();
    this.getAllCategory();
  
  }

  modalClose() {
    this.modalRef?.hide();
  }

  openModal(template: TemplateRef<any>, isEditing: boolean) {
    this.modalRef = this.modalService.show(template);
    this.clear();
    console.log('this.modalRef', this.modalRef);
    this.isEditing = isEditing;
  }

  @HostListener('window:resize')
  adjustTableHeight() {
    const windowHeight = window.innerHeight;
    const container = document.querySelector('.table-container') as HTMLElement;
    const containerRect = container.getBoundingClientRect();
    const containerTopOffset = containerRect.top;
    const containerBottomOffset = containerRect.bottom;

    const availableHeight =
      windowHeight - containerTopOffset - containerBottomOffset;
    container.style.height = `${availableHeight}px`;
  }

  expertForm() {
    this.categoryForm = this.fb.group({
      categoryName: ['', Validators.required],
      description: [ '', Validators.required],
     
    });
  }

  getAllCategory() {
    // this.loaderService.showLoader()
    this.httpService.get('getAllCategory').subscribe(
      (res) => {
        this.categoryDetails = res.data;
        console.log('this.categoryDetails', this.categoryDetails)
        // this.loaderService.hideLoader();
      },
      (error) => {
        console.log('error', error);
        // this.loaderService.hideLoader();
      }
    )
  }
  
  createCategory() {
    console.log('this.categoryForm.value', this.categoryForm.value)
    this.httpService.post('createCategory', this.categoryForm.value).subscribe(
      (res) => {
        this.clear();
        console.log('res', res);
        this.toastr.success('Add Category Successfully');
        
        this.categoryForm.reset();
        this.getAllCategory();
      },

      (error) => {
        console.log('error', error)
        this.toastr.error(error?.error?.messages.error);
        // const errorMessage = error?.messages?.error;
        // if (errorMessage) {
        //   this.toastr.error(errorMessage);
        // } else {
        //   this.toastr.error("An error occurred."); // Fallback message
        // }
      }
    )
  }

  openEditModal(id: any, template: any,isEditing: boolean) {
    console.log('id', id)
    this.singleCategoryId = this.categoryDetails.find((x: any) => x.categoryId == id);
    console.log('this.singleCategoryId', this.singleCategoryId);
    this.isEditing = isEditing;
    this.openModal(template ,isEditing);
    this.patchSignleValueTask(this.singleCategoryId);
  }

  updateCategoryByCategoryId() {
    this.httpService
      .put(
        'updateCategoryByCategoryId',
        this.singleCategoryId.categoryId ,
        this.categoryForm.value
      )
      .subscribe({
        next: (response) => {
          console.log('response', response);
          this.toastr.success('Update category Successfully');
          this.getAllCategory();
          this.categoryForm.reset();
          this.modalRef?.hide();
        },
        error: (err) => {
          console.log('err', err);
          this.toastr.error(err?.error?.messages.error);
          // const errorMessage = err?.messages?.error;
          // if (errorMessage) {
          //   this.toastr.error(errorMessage);
          // } else {
          //   this.toastr.error("An error occurred."); // Fallback message
          // }
        },
        complete: () => {
          console.info('complete.!!');
        },
      });
  }


  patchSignleValueTask(singleCategoryId: any) {
    console.log('singleCategoryId', singleCategoryId)
    this.categoryForm.get('categoryName')?.patchValue(singleCategoryId.categoryName);
    this.categoryForm.get('description')?.patchValue(singleCategoryId.description);
  }

  submitForm() {
    if (this.categoryForm.valid) {
      const formData = this.categoryForm.value;
      console.log(formData);
    } else {
    }
  }

  deleteCategoryById(id: any) {
    Swal.fire({
      title: 'Delete',
      text: "Are you sure?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it'
    }).then((result) => {
      if (result.isConfirmed) {
        this.httpService.delete('deleteCategoryById', id).subscribe(
          (res: any) => {
            console.log('res', res);
            this.getAllCategory();
          },
          (error: any) => {
            console.log('error', error);
          }
        );
      }
    });
  }
 

/*   deleteCategoryById(id: any) {
    this.httpService.delete('deleteCategoryById', id).subscribe(
      (res : any) => {
        console.log('res', res)
        this.getAllCategory();
      },
      (error : any) => {
        console.log('error', error)
      }
    )
  } */

  clear() {
    this.categoryForm.reset();
  }

  getData(): any[] {
    // Replace this with your actual data retrieval logic
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    return this.categoryDetails?.slice(startIndex, endIndex);
  }

  get totalPages(): number {
    return Math.ceil(this.categoryDetails?.length / this.itemsPerPage);
  }

  get pages(): number[] {
    return Array(this.totalPages)
      .fill(0)
      .map((_, i) => i + 1);
  }

  updateCurrentPage(page: number): void {
    if (page >= 1 && page <= this.totalPages) {
      this.currentPage = page;
    }
  }

  getPages(): number[] {
    const totalPages = this.getTotalPages();
    return Array(totalPages)
      .fill(0)
      .map((_, index) => index + 1);
  }

  getTotalPages(): number {
    return Math.ceil(this.categoryDetails?.length / this.itemsPerPage);
  }

}
