import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/shared/services/http/http.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
  @ViewChild('productImageInput') productImageInput: ElementRef;
  files2: File[] = [];
  productForm: FormGroup;
  productData: any;
  showSubmitButton = true;
  selectedId: any;
  receviedData: any;
  productDetials: any;
  productId: any;
  selectedImage: File | undefined;
  singleProductId: any;
  selectedImages: File[] = [];
  categoryId: string;
  categoryDetails: any[];
  uploadedImageUrl: any;
  uploadedImageUrl1: string | null = null;
  productImageSrc: any;
  selectedcategoryId: any;

  constructor(private fb: FormBuilder, private httpService: HttpService, public router: Router, private http : HttpClient,private cdRef: ChangeDetectorRef) {
    this.receviedData = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    this.expertForm();
    console.log('this.receviedData', this.receviedData)
      this.categoryDetails = []; // Initialize or fetch the actual categoryDetails array
  console.log('this.categoryDetails', this.categoryDetails);

  }

  ngOnInit(): void { 
   this.getProductList();
   this.getAllCategory();
   
  }


  expertForm(){
    this.productForm = this.fb.group({
      categoryId: new FormControl(),
      productName: [this.receviedData ? this.receviedData.productName :'', Validators.required],
      productQty: [ this.receviedData ? this.receviedData.productQty :'', Validators.required],
      productUnit: [this.receviedData ? this.receviedData.productUnit :'', Validators.required],
      productPrice: [this.receviedData ? this.receviedData.productPrice :'', Validators.required],
      productDescription: [this.receviedData ? this.receviedData.productDescription :'', Validators.required],
      discount: [this.receviedData ? this.receviedData.discount :'', Validators.required],
      deliveryCharges: [this.receviedData ? this.receviedData.deliveryCharges :'', Validators.required],
      productImage:  [this.receviedData ? this.receviedData.productImage : '', Validators.required],
    });
  }


  submitForm() {
    if (this.productForm.valid) {
      const formData = this.productForm.value;
      console.log(formData);
    } else {
      // Handle form validation errors
    }
  }

  changecategoryId(){
    if (this.productForm) {
      const selectedCategoryId = this.productForm.get('categoryId')?.value;
      console.log('Selected Category ID:', selectedCategoryId);
    }
  }
  

  getAllCategory() {
    // this.loaderService.showLoader()
    this.httpService.get('getAllCategory').subscribe(
      (res) => {
        this.categoryDetails = res.data;
        console.log('this.categoryDetails', this.categoryDetails)
        if (this.categoryDetails.length > 0) {
          this.categoryId = this.categoryDetails[0].categoryId;
          console.log('this.categoryId', this.categoryId);
        }
        // this.pagination = res.pagination;
        // this.loaderService.hideLoader();
      },
      (error) => {
        console.log('error', error);
        // this.loaderService.hideLoader();
      }
    )
  }


  getProductList() {
    this.httpService.get('getProductList').subscribe(
      (res) => {
        this.productDetials = res.data;
        console.log('this.productDetials', this.productDetials)
      },
      (error) => {
        console.log('error', error);
      }
    )
  }

  onImageSelect(event: any) {
    if (event.target.files && event.target.files.length) {
      for (let i = 0; i < event.target.files.length; i++) {
        this.selectedImages.push(event.target.files[i]);
      }
    }
  }


  createProduct() {
    let sendData = {
      categoryId:  this.productForm.value.categoryId,
      productName: this.productForm.value.productName,
      productQty: this.productForm.value.productQty,
      productUnit: this.productForm.value.productUnit,
      productPrice: this.productForm.value.productPrice,
      productDescription: this.productForm.value.productDescription,
      discount: this.productForm.value.discount,
      deliveryCharges: this.productForm.value.deliveryCharges,
      productImage: this.uploadedImageUrl,
    };

    this.http.post('https://shakvatika.chargurev.com/createProduct', sendData).subscribe(
      (res) => {
        this.clear();
        console.log('res', res);
        this.productForm.reset();
        this.getProductList();
      },
      (error) => {
        console.log('error', error);
      }
    );
  }

  uploadProductImage(event: any): void {
    const files = event.target.files;
    if (files && files.length > 0) {
      const formData = new FormData();

      for (const file of files) {
        formData.append('imageFile', file, file.name);
      }

      this.http.post('https://shakvatika.chargurev.com/uploadProductImage', formData)
        .subscribe(
          (response) => {
            console.log('Upload successful:', response);
             this.uploadedImageUrl = response; 
             this.productImageSrc = response;
          },
          (error) => {
            console.error('Upload error:', error);
          }
        );
    }
  }



  openEditModal(id: any) {
    console.log('id', id)
    // this.isEditCLicked = true;
    this.productId = this.productDetials.find((x: any) => x.productId == id);
    console.log('this.singleProductId', this.productId)
    // this.openModal(template);
    // this.patchSignleValueTask(this.productId);
  }

  // updateProductByProductId() {
  //     const formData = new FormData();
  //     formData.append('productName', this.productForm.value.productName);
  //     formData.append('productQty', this.productForm.value.productQty);
  //     formData.append('productPrice', this.productForm.value.productPrice);
  //     formData.append('productUnit', this.productForm.value.productUnit);
  //     formData.append('productDescription', this.productForm.value.productDescription);
  //     formData.append('productImage', this.productForm.value.productImage);
  
  //     this.httpService.put('updateProductByProductId',this.singleProductId, formData).subscribe(
  //       (response) => {
  //         // Handle success, reset form, fetch updated data, etc.
  //       },
  //       (error) => {
  //         console.error('Error updating product:', error);
  //       }
  //     );
  // }  

  updateProductByProductId() {
    this.httpService
      .put(
        'updateProductByProductId',
        this.productId,
        this.productForm.value
      )
      .subscribe({
        next: (response) => {
          console.log('response', response);
          this.getProductList();
          this.productForm.reset();
          // this.modalRef?.hide();
          // this.isEditCLicked = false;
        },
        error: (err) => {
          console.log('err', err);
        },
        complete: () => {
          console.info('complete.!!');
        },
      });
  }


  patchSignleValueTask(singleProductId: any) {
    console.log('singleProductId', singleProductId)
    this.productForm.get('categoryId')?.patchValue(singleProductId.categoryId);
    this.productForm.get('productName')?.patchValue(singleProductId.productName);
    this.productForm.get('productQty')?.patchValue(singleProductId.productQty);
    this.productForm.get('productUnit')?.patchValue(singleProductId.productUnit);
    this.productForm.get('productPrice')?.patchValue(singleProductId.productPrice);
    this.productForm.get('discount')?.patchValue(singleProductId.discount);
    this.productForm.get('deliveryCharges')?.patchValue(singleProductId.deliveryCharges);
    this.productForm.get('productDescription')?.patchValue(singleProductId.productDescription);
    // this.productImageSrc = singleProductId.productImage
  }

  clear() {
    this.productForm.reset();
  }




  formBuild() {
    this.productForm = this.fb.group({
      productName: [this.receviedData ? this.receviedData.productName : '', Validators.required],
      productDescription: [this.receviedData ? this.receviedData.productDescription : '', Validators.required],
      productCategory: [this.receviedData ? this.receviedData.productCategory : '', Validators.required],
      productQty: [this.receviedData ? this.receviedData.productQty : '', [Validators.required, Validators.pattern("^[0-9]*$")]],
      productUnit: [this.receviedData ? this.receviedData.productUnit : '', Validators.required],
      expiryDate: [this.receviedData ? this.receviedData.expiryDate : '', Validators.required],
      originalPrice: [this.receviedData ? this.receviedData.originalPrice : '', [Validators.required, Validators.pattern("^[0-9]*$")]],
      offerPrice: [this.receviedData ? this.receviedData.offerPrice : '', Validators.required],
      offerPercentage: [this.receviedData ? this.receviedData.offerPercentage : '', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.max(100)]],
      productDelivery: [this.receviedData ? this.receviedData.productDelivery : '', Validators.required]
    });
    // this.productForm.valueChanges.subscribe(() => this.calculateOfferPrice());
    this.productForm.controls['originalPrice'].valueChanges.subscribe(change => {
      this.calculateOfferPrice()
    });
    this.productForm.controls['offerPercentage'].valueChanges.subscribe(change => {
      this.calculateOfferPrice()
    });
  }




  calculateOfferPrice() {
    console.log('calculateOfferPrice')
    const originalPrice1 = this.productForm.get('originalPrice')?.value;
    const offerPercentage1 = this.productForm.get('offerPercentage')?.value;
    if (originalPrice1 != null && offerPercentage1 != null) {
      const offerPrice = originalPrice1 - (originalPrice1 * offerPercentage1 / 100);
      this.productForm.get('offerPrice')?.setValue(offerPrice);
    }
  }

  onSelect2(event: any) {
    this.files2.push(...event.addedFiles);
  }

  onRemove2(event: any) {
    this.files2.splice(this.files2.indexOf(event), 1);
  }


  onSubmit() {
    if (this.productForm.invalid) {
      return;
    }
    this.addProductFromData();
  }

  // getProductList() {
  //   this.httpService.get('product/getProductList').subscribe(
  //     (res) => {
  //       console.log('res', res)
  //       this.productData = res.data;
  //     },

  //     (error) => {
  //       console.log('error', error)
  //     }
  //   )
  // }

  addProductFromData() {
    this.httpService.post('product/addProductFromData', this.productForm.value).subscribe(
      (res) => {
        console.log('res', res)
        this.getProductList();
        this.productForm.reset();
      },

      (error) => {
        console.log('error', error)
      }
    )
  }

  edit(item: any) {
    this.selectedId = item._id;
    this.productForm.patchValue({
      productType: item.productType,
      productName: item.productName,
      productQty: item.productQty,
      productUnit: item.productUnit,
    });
    this.showSubmitButton = false
  }

  updateProductList() {
    this.httpService.put('product/updateProduct', this.selectedId, this.productForm.value).subscribe(
      (res) => {
        console.log('res', res)
        this.getProductList();
        this.showSubmitButton = true;
        this.productForm.reset();
      },

      (error) => {
        console.log('error', error)
      }
    )
  }
}
