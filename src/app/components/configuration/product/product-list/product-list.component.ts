import { DecimalPipe } from '@angular/common';
import { Component, ElementRef, HostListener, QueryList, TemplateRef, ViewChild, ViewChildren, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductListService } from 'src/app/shared/services/ecommerce/product-list.service';
import { NavigationExtras, Router } from '@angular/router';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader/loader.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
  providers: [ProductListService, DecimalPipe]
})
export class ProductListComponent {
  @ViewChild('productImageInput') productImageInput: ElementRef;
  searchText: any = "";
  pagination = {
    limit: 5,
    currentPage: 1,
    totalCount: 0,
    hasNextPage: false,
    hasPrevPage: false,
  }
  productForm: FormGroup;
  productDetials: any = [];
  productId: string | null;
  modalRef?: BsModalRef;
  singleProductId: any;
  categoryDetails: any;
  categoryId: string;
  selectedcategoryId: any;
  uploadedImageUrl: any;
  isEditing: boolean = false;
  productImageFile: File | null = null;
  productImageSrc: string | null = null;
  currentPage: any = 1;
  itemsPerPage = 5;

  constructor(
    private httpService: HttpService,
    private loaderService: LoaderService,
    private router: Router,
    private modalService: BsModalService,
    private fb: FormBuilder,
    private http: HttpClient,
    private toastr: ToastrService,
  ) {
    /*   this.qParams = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
      console.log('this.qParams', this.qParams) */
    this.expertForm();
  }

  ngOnInit(): void {
    // this.productId = this.productDetials;
    // console.log('this.productId', this.productId)
    this.getProductList();
    this.getAllCategory();
  }

  @HostListener('window:resize')
  adjustTableHeight() {
    const windowHeight = window.innerHeight;
    const container = document.querySelector('.table-container') as HTMLElement;
    const containerRect = container.getBoundingClientRect();
    const containerTopOffset = containerRect.top;
    const containerBottomOffset = containerRect.bottom;

    const availableHeight =
      windowHeight - containerTopOffset - containerBottomOffset;
    container.style.height = `${availableHeight}px`;
  }

  expertForm() {
    this.productForm = this.fb.group({
      categoryId: new FormControl(),
      productName: ['', Validators.required],
      productQty: ['', Validators.required],
      productUnit: ['', Validators.required],
      productPrice: ['', Validators.required],
      discount: ['', Validators.required],
      deliveryCharges: ['', Validators.required],
      productDescription: ['', Validators.required],
      productImage: ['', Validators.required],
    });
  }

  addData(){
    this.router.navigate(['/configuration/add-product'])
  }
  changecategoryId() {
    if (this.productForm) {
      const selectedCategoryId = this.productForm.get('categoryId')?.value;
      console.log('Selected Category ID:', selectedCategoryId);
    }
  }


  getAllCategory() {
    // this.loaderService.showLoader()
    this.httpService.get('getAllCategory').subscribe(
      (res) => {
        this.categoryDetails = res.data;
        console.log('this.categoryDetails', this.categoryDetails)
        if (this.categoryDetails.length > 0) {
          this.categoryId = this.categoryDetails[0].categoryId;
          console.log('this.categoryId', this.categoryId);
        }
        // this.pagination = res.pagination;
        // this.loaderService.hideLoader();
      },
      (error) => {
        console.log('error', error);
        // this.loaderService.hideLoader();
      }
    )
  }

  openModal(template: TemplateRef<any>, isEditing: boolean) {
    this.modalRef = this.modalService.show(template);
    console.log('this.modalRef', this.modalRef)
    this.clear();
    this.isEditing = isEditing;
  }

  getProductList() {
    this.httpService.get('getProductList').subscribe(
      (res) => {
        this.productDetials = res.data;
        console.log('this.productDetials', this.productDetials)
      },
      (error) => {
        console.log('error', error);
      }
    )
  }


  createProduct() {
    let sendData = {
      categoryId: this.productForm.value.categoryId,
      productName: this.productForm.value.productName,
      productQty: this.productForm.value.productQty,
      productUnit: this.productForm.value.productUnit,
      productPrice: this.productForm.value.productPrice,
      productDescription: this.productForm.value.productDescription,
      discount: this.productForm.value.discount,
      deliveryCharges: this.productForm.value.deliveryCharges,
      productImage: this.uploadedImageUrl,
    };

    this.http.post('https://shakvatika.chargurev.com/createProduct', sendData).subscribe(
      (res) => {
        this.clear();
        console.log('res', res);
        this.toastr.success('Add product Successfully');
        this.uploadedImageUrl = null;
        this.productForm.reset();
        this.getProductList();
      },
      (error) => {
        console.log('error', error);
        this.toastr.error(error?.error?.messages.error);
        // const errorMessage = error?.messages?.error;
        // if (errorMessage) {
        //   this.toastr.error(errorMessage);
        // } else {
        //   this.toastr.error("An error occurred."); // Fallback message
        // }
      }
    );
  }


  uploadProductImage(event: any): void {
    const files = event.target.files;
    if (files && files.length > 0) {
      const formData = new FormData();

      for (const file of files) {
        formData.append('imageFile', file, file.name);
      }

      this.http.post('https://shakvatika.chargurev.com/uploadProductImage', formData)
        .subscribe(
          (response) => {
            console.log('Upload successful:', response);
            this.uploadedImageUrl = response;
            this.productImageFile = files[0]; // Store the selected file
            this.productImageSrc = response as string;
          },
          (error) => {
            console.error('Upload error:', error);
          }
        );
    }
  }


  editTheForm(item: any,) {
    console.log('goProductDetails')
    let objToSend: NavigationExtras = {
      queryParams: item,
      skipLocationChange: false,
      fragment: 'top'
    };

    this.router.navigate(['/configuration/add-product'], { state: objToSend });
  }

  openEditModal(id: any, template: any, isEditing: boolean) {
    console.log('id', id)
    // this.isEditCLicked = true;
    this.singleProductId = this.productDetials.find((x: any) => x.productId == id);
    this.selectedcategoryId = this.singleProductId.category?.categoryId;
    console.log('this.singleProductId', this.singleProductId);
    this.isEditing = isEditing;
    this.openModal(template, isEditing);
    this.patchSignleValueTask(this.singleProductId);
  }

  updateProductByProductId() {
    // Ensure that this.uploadedImageUrl contains the correct image URL
    const updatedProductImage = this.uploadedImageUrl || this.singleProductId.productImage;
  
    const sendData = {
      categoryId: this.productForm.value.categoryId,
      productName: this.productForm.value.productName,
      productQty: this.productForm.value.productQty,
      productUnit: this.productForm.value.productUnit,
      productPrice: this.productForm.value.productPrice,
      productDescription: this.productForm.value.productDescription,
      discount: this.productForm.value.discount,
      deliveryCharges: this.productForm.value.deliveryCharges,
      productImage: updatedProductImage, // Use updatedProductImage here
    };
  
    this.httpService.put(
      'updateProductByProductId',
      this.singleProductId.productId,
      sendData
    ).subscribe({
      next: (response) => {
        console.log('response', response);
        this.toastr.success('Update product Successfully');
        this.getProductList();
        this.productForm.reset();
        this.modalRef?.hide();
      },
      error: (err) => {
        console.log('err', err);
        this.toastr.error(err?.error?.messages.error);
        // const errorMessage = err?.messages?.error;
        // if (errorMessage) {
        //   this.toastr.error(errorMessage);
        // } else {
        //   this.toastr.error("An error occurred.");
        // }
      },
      complete: () => {
        console.info('complete.!!');
      },
    });
  }
  

  /* updateProductByProductId() {
    const sendData = {
      categoryId: this.productForm.value.categoryId,
      productName: this.productForm.value.productName,
      productQty: this.productForm.value.productQty,
      productUnit: this.productForm.value.productUnit,
      productPrice: this.productForm.value.productPrice,
      productDescription: this.productForm.value.productDescription,
      discount: this.productForm.value.discount,
      deliveryCharges: this.productForm.value.deliveryCharges,
      productImage: this.uploadedImageUrl,
    };

    this.httpService
      .put(
        'updateProductByProductId',
        this.singleProductId.productId,
        sendData
      )
      .subscribe({
        next: (response) => {
          console.log('response', response);
          this.toastr.success('Update product Successfully');
          this.getProductList();
          this.productForm.reset();
          this.modalRef?.hide();
          // this.isEditCLicked = false;
        },
        error: (err) => {
          console.log('err', err);
          const errorMessage = err?.messages?.error;
          if (errorMessage) {
            this.toastr.error(errorMessage);
          } else {
            this.toastr.error("An error occurred."); // Fallback message
          }
        },
        complete: () => {
          console.info('complete.!!');
        },
      });
  } */

  patchSignleValueTask(singleProductId: any) {
    console.log('singleProductId', singleProductId)
    this.selectedcategoryId = this.singleProductId.category?.categoryId;
    this.productForm.get('productName')?.patchValue(singleProductId.productName);
    this.productForm.get('productQty')?.patchValue(singleProductId.productQty);
    this.productForm.get('productUnit')?.patchValue(singleProductId.productUnit);
    this.productForm.get('productPrice')?.patchValue(singleProductId.productPrice);
    this.productForm.get('discount')?.patchValue(singleProductId.discount);
    this.productForm.get('deliveryCharges')?.patchValue(singleProductId.deliveryCharges);
    this.productForm.get('productDescription')?.patchValue(singleProductId.productDescription);
    this.productForm.get('productImage')?.patchValue(singleProductId.productImage);
    this.productImageSrc = singleProductId.productImage;
  }

  deleteProductById(id: any) {
    Swal.fire({
      title: 'Delete',
      text: "Are you sure?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it'
    }).then((result) => {
      if (result.isConfirmed) {
        this.httpService.delete('deleteProductById', id).subscribe(
          (res: any) => {
            console.log('res', res);
            this.getProductList();
          },
          (error: any) => {
            console.log('error', error);
          }
        );
      }
    });
  }

 /*  deleteProductById(i: any) {
    this.httpService.delete('deleteProductById', i).subscribe(
      (res) => {
        console.log('res', res)
        this.getProductList();
      },

      (error) => {
        console.log('error', error)
      }
    )
  } */

  submitForm() {
    if (this.productForm.valid) {
      const formData = this.productForm.value;
      console.log(formData);
    } else {
      // Handle form validation errors
    }
  }

  clear() {
    this.productForm.reset();
    this.productImageSrc = null; // Clear the image URL
    this.productImageFile = null; // Clear the image file
    this.selectedcategoryId = null; // Clear the image file
  }

  getData(): any[] {
    // Replace this with your actual data retrieval logic
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    return this.productDetials?.slice(startIndex, endIndex);
  }

  get totalPages(): number {
    return Math.ceil(this.productDetials?.length / this.itemsPerPage);
  }

  get pages(): number[] {
    return Array(this.totalPages)
      .fill(0)
      .map((_, i) => i + 1);
  }

  updateCurrentPage(page: number): void {
    if (page >= 1 && page <= this.totalPages) {
      this.currentPage = page;
    }
  }

  getPages(): number[] {
    const totalPages = this.getTotalPages();
    return Array(totalPages)
      .fill(0)
      .map((_, index) => index + 1);
  }

  getTotalPages(): number {
    return Math.ceil(this.productDetials?.length / this.itemsPerPage);
  }


  // products$: Observable<productList[]>;
  // total$: Observable<number>;
  // public PRODUCTLIST = data.PRODUCTLIST;
  // searchText: any = "";
  // pagination = {
  //   limit: 5,
  //   currentPage: 1,
  //   totalCount: 0,
  //   hasNextPage: false,
  //   hasPrevPage: false,
  // }


  // @ViewChildren(ProductListDirective)
  // headers!: QueryList<ProductListDirective>;
  // productData: any;

  // constructor(public service: ProductListService, private loaderService: LoaderService, public router: Router, private httpService: HttpService) {
  //   this.products$ = service.support$;
  //   this.total$ = service.total$;
  //   this.getProductList();
  // }

  // onSort({ column, direction }: SortEvent) {
  //   // resetting other headers
  //   this.headers.forEach(header => {
  //     if (header.sortable !== column) {
  //       header.direction = '';
  //     }
  //   });

  //   this.service.sortColumn = column;
  //   this.service.sortDirection = direction;
  // }

  // get filteredItems() {
  //   return this.productData?.filter((item: any) => item.productName.includes(this.searchText) || item.productQty.includes(this.searchText));
  // }

  // goProductDetails() {
  //   console.log('goProductDetails')
  //   this.router.navigate(["/configuration/product-details"]);
  // }

  // editTheForm(item: any) {
  //   console.log('goProductDetails')
  //   let objToSend: NavigationExtras = {
  //     queryParams: item,
  //     skipLocationChange: false,
  //     fragment: 'top'
  //   };

  //   this.router.navigate(['/configuration/add-product'], { state: objToSend });
  // }

  // getProductList() {
  //   this.loaderService.showLoader()
  //   this.httpService.get('product/getProductList', this.pagination.currentPage, this.pagination.limit).subscribe(
  //     (res) => {
  //       this.productData = res.data;
  //       console.log('res', res)
  //       this.pagination = res.pagination;
  //       this.loaderService.hideLoader();
  //     },

  //     (error) => {
  //       console.log('error', error)
  //       this.loaderService.hideLoader()
  //     }
  //   )
  // }



  // deleteProductList(i: any) {
  //   this.httpService.delete('product/deleteProductList', i).subscribe(
  //     (res) => {
  //       console.log('res', res)
  //       this.getProductList();
  //     },

  //     (error) => {
  //       console.log('error', error)
  //     }
  //   )
  // }
}
