import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader/loader.service';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent {
  searchText: any = "";

  pagination = {
    limit: 5,
    currentPage: 1,
    totalCount: 0,
    hasNextPage: false,
    hasPrevPage: false,
  }
  expertProfileData: any;
  showSubmitButton: boolean;
  selectedId: any;
  roleDetails: any;
  currentPage : any = 1;
  itemsPerPage = 5;

  constructor(
    private httpService: HttpService,
    private loaderService: LoaderService,
    private router: Router) {

  }

  ngOnInit(): void {
    this.getAllRoleList();
  }

  

  get filteredItems() {
    return this.expertProfileData?.filter((item: any) => item.name.includes(this.searchText) || item.location.includes(this.searchText));
  }

  getAllRoleList() {
    // this.loaderService.showLoader()
    this.httpService.get('getAllRoleList').subscribe(
      (res :any ) => {
        // console.log('res', res)
        this.roleDetails = res.data;
        console.log('roleDetails', this.roleDetails)
        // this.pagination = res.pagination;
        // this.loaderService.hideLoader();
      },

      (error : any) => {
        console.log('error', error);
        // this.loaderService.hideLoader();
      }
    )
  }


  getDemoDataList() {
    // this.loaderService.showLoader()
    this.httpService.get('posts').subscribe(
      (res :any ) => {
        // console.log('res', res)
        this.expertProfileData = res;
        console.log('this.expertProfileData', this.expertProfileData)
        // this.pagination = res.pagination;
        // this.loaderService.hideLoader();
      },

      (error : any) => {
        console.log('error', error);
        // this.loaderService.hideLoader();
      }
    )
  }

  edit(item: any) {
    console.log('item', item)
    let objToSend: NavigationExtras = {
      queryParams: item,
      skipLocationChange: false,
      fragment: 'top'
    };
    this.router.navigate(['/configuration/demo'], { state: objToSend });
  }

  deleteExpertProfileList(i: any) {
    this.httpService.delete('posts', i).subscribe(
      (res : any) => {
        console.log('res', res)
        this.getDemoDataList();
      },

      (error : any) => {
        console.log('error', error)
      }
    )
  }

  getData(): any[] {
    // Replace this with your actual data retrieval logic
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    return this.roleDetails?.slice(startIndex, endIndex);
  }

  get totalPages(): number {
    return Math.ceil(this.roleDetails?.length / this.itemsPerPage);
  }

  get pages(): number[] {
    return Array(this.totalPages)
      .fill(0)
      .map((_, i) => i + 1);
  }

  updateCurrentPage(page: number): void {
    if (page >= 1 && page <= this.totalPages) {
      this.currentPage = page;
    }
  }

  getPages(): number[] {
    const totalPages = this.getTotalPages();
    return Array(totalPages)
      .fill(0)
      .map((_, index) => index + 1);
  }

  getTotalPages(): number {
    return Math.ceil(this.roleDetails?.length / this.itemsPerPage);
  }

}
