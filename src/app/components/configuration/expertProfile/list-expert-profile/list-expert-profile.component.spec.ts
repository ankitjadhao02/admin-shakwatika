import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListExpertProfileComponent } from './list-expert-profile.component';

describe('ListExpertProfileComponent', () => {
  let component: ListExpertProfileComponent;
  let fixture: ComponentFixture<ListExpertProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListExpertProfileComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListExpertProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
