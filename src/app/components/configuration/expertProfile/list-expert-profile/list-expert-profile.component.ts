import { Component, HostListener, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader/loader.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-expert-profile',
  templateUrl: './list-expert-profile.component.html',
  styleUrls: ['./list-expert-profile.component.scss']
})
export class ListExpertProfileComponent {
  searchText: any = "";
  ExpertProfileForm: FormGroup;
  isEditing: boolean = false;
  selectedId: any;
  expertProfileData: any = [];
  modalRef?: BsModalRef;
  singleExpertId: any;
  currentPage : any = 1;
  itemsPerPage = 5;

  constructor(private httpService: HttpService, private loaderService: LoaderService, private router: Router,private fb: FormBuilder,private modalService: BsModalService,    private toastr: ToastrService,) {
    this.ExpertProfileForm = this.fb.group({
      name: ['', Validators.required],
      backgGround: ['', Validators.required],
      contactNo: ['', [Validators.required, Validators.pattern(/^\d{10}$/)]],
    });

  }

  ngOnInit(): void {
    this.getExpertProfileList();
  }

  @HostListener('window:resize')
  adjustTableHeight() {
    const windowHeight = window.innerHeight;
    const container = document.querySelector('.table-container') as HTMLElement;
    const containerRect = container.getBoundingClientRect();
    const containerTopOffset = containerRect.top;
    const containerBottomOffset = containerRect.bottom;

    const availableHeight =
      windowHeight - containerTopOffset - containerBottomOffset;
    container.style.height = `${availableHeight}px`;
  }

  openModal(template: TemplateRef<any>,isEditing: boolean) {
    this.modalRef = this.modalService.show(template);
    this.clear();
    console.log('this.modalRef', this.modalRef);
    this.isEditing = isEditing;
  }

  submit() {
    if (this.ExpertProfileForm.valid) {
      const formData = this.ExpertProfileForm.value;
      console.log(formData);
    } else {
      // Handle form validation errors
    }
  }

  getExpertProfileList() {
    this.httpService.get('getExpertProfileList').subscribe(
      (res) => {
        console.log('res', res)
        this.expertProfileData = res.data;
      },

      (error) => {
        console.log('error', error);
      }
    )
  }

  createExpertProfile(){
    console.log('this.userForm.value', this.ExpertProfileForm.value)
    this.httpService.post('createExpertProfile', this.ExpertProfileForm.value).subscribe(
      (res) => {
        this.toastr.success('Add Expert Successfully');
        this.clear();
        console.log('res', res);
        // this.validate = !this.validate;
        this.ExpertProfileForm.reset();
        this.getExpertProfileList();
      },

      (error) => {
        console.log('error', error);
        this.toastr.error(error?.error?.messages.error);
        // const errorMessage = error?.messages?.error;
        // if (errorMessage) {
        //   this.toastr.error(errorMessage);
        // } else {
        //   this.toastr.error("An error occurred."); // Fallback message
        // }
      }
    )
  }

  openEditModal(id: any, template: any,isEditing: boolean) {
    console.log('id', id)
    // this.isEditCLicked = true;
    this.singleExpertId = this.expertProfileData.find((x: any) => x.id == id);
    console.log('this.singleExpertId', this.singleExpertId)
    this.isEditing = isEditing;
    this.openModal(template ,isEditing);
    this.patchSignleValueTask(this.singleExpertId);
  }

  updateExpertProfile(){
    this.httpService
      .put(
        'updateExpertProfile',
        this.singleExpertId.id,
        this.ExpertProfileForm.value
      )
      .subscribe({
        next: (response) => {
          this.toastr.success('Update Expert Successfully');
          console.log('response', response);
          this.getExpertProfileList();
          this.ExpertProfileForm.reset();
          this.modalRef?.hide();
          // this.isEditCLicked = false;
        },
        error: (err) => {
          console.log('err', err);
          this.toastr.error(err?.error?.messages.error);
          // const errorMessage = err?.messages?.error;
          // if (errorMessage) {
          //   this.toastr.error(errorMessage);
          // } else {
          //   this.toastr.error("An error occurred."); // Fallback message
          // }
        },
        complete: () => {
          console.info('complete.!!');
        },
      });
  }

  patchSignleValueTask(singleExpertId: any) {
    console.log('singleExpertId', singleExpertId)
    this.ExpertProfileForm.get('name')?.patchValue(singleExpertId.name);
    this.ExpertProfileForm.get('backgGround')?.patchValue(singleExpertId.backgGround);
    this.ExpertProfileForm.get('contactNo')?.patchValue(singleExpertId.contactNo);
  }

  deleteExpertProfileById(id: any) {
    Swal.fire({
      title: 'Delete',
      text: "Are you sure?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it'
    }).then((result) => {
      if (result.isConfirmed) {
        this.httpService.delete('deleteExpertProfileById', id).subscribe(
          (res: any) => {
            console.log('res', res);
            this.getExpertProfileList();
          },
          (error: any) => {
            console.log('error', error);
          }
        );
      }
    });
  }

/*   deleteExpertProfileById(id: any) {
    this.httpService.delete('deleteExpertProfileById', id).subscribe(
      (res : any) => {
        console.log('res', res)
        this.getExpertProfileList();
      },
      (error : any) => {
        console.log('error', error)
      }
    )
  } */

  clear() {
    this.ExpertProfileForm.reset();
  }

  getData(): any[] {
    // Replace this with your actual data retrieval logic
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    return this.expertProfileData?.slice(startIndex, endIndex);
  }

  get totalPages(): number {
    return Math.ceil(this.expertProfileData?.length / this.itemsPerPage);
  }

  get pages(): number[] {
    return Array(this.totalPages)
      .fill(0)
      .map((_, i) => i + 1);
  }

  updateCurrentPage(page: number): void {
    if (page >= 1 && page <= this.totalPages) {
      this.currentPage = page;
    }
  }

  getPages(): number[] {
    const totalPages = this.getTotalPages();
    return Array(totalPages)
      .fill(0)
      .map((_, index) => index + 1);
  }

  getTotalPages(): number {
    return Math.ceil(this.expertProfileData?.length / this.itemsPerPage);
  }


}


