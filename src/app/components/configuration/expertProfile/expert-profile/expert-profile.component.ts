import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader/loader.service';

@Component({
  selector: 'app-expert-profile',
  templateUrl: './expert-profile.component.html',
  styleUrls: ['./expert-profile.component.scss']
})
export class ExpertProfileComponent {

  public validate = false;
  public tooltipValidation = false;
  expertProfileForm: FormGroup;
  expertProfileData: any;
  showSubmitButton = true;
  selectedId: any;
  params: any;
  searchText: any = "";

  pagination = {
    limit: 5,
    currentPage: 1,
    totalCount: 0,
    hasNextPage: false,
    hasPrevPage: false,
  }
  qParams: any;


  constructor(private httpService: HttpService, private fb: FormBuilder, private loaderService: LoaderService, private router: Router) {
    this.qParams = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    this.expertForm();
  }

  ngOnInit(): void {
    this.getExpertProfileList();
  }

  expertForm() {
    this.expertProfileForm = this.fb.group({
      name: [this.qParams ? this.qParams.name : '', Validators.required],
      backGround: [this.qParams ? this.qParams.backGround : '', Validators.required],
      experience: [this.qParams ? this.qParams.experience : '', Validators.required],
      mobileNo: [this.qParams ? this.qParams.mobileNo : '', [Validators.required, Validators.pattern(/^\d{10}$/)]],
      location: [this.qParams ? this.qParams.location : '', Validators.required]
    });
  }

  public submit() {
    this.validate = !this.validate;
    if (this.expertProfileForm.invalid) {
      return;
    }
    this.addExpertProfileFromData();
  }

  get filteredItems() {
    return this.expertProfileData?.filter((item: any) => item.name.includes(this.searchText) || item.location.includes(this.searchText));
  }

  getExpertProfileList() {
    this.loaderService.showLoader()
    this.httpService.get('expertProfile/getExpertProfileList').subscribe(
      (res) => {
        console.log('res', res)
        this.expertProfileData = res.data;
        this.pagination = res.pagination;
        this.loaderService.hideLoader();
      },

      (error) => {
        console.log('error', error);
        this.loaderService.hideLoader();
      }
    )
  }

  addExpertProfileFromData() {
    console.log('this.expertProfileForm.value', this.expertProfileForm.value)
    this.httpService.post('expertProfile/addExpertProfileFromData', this.expertProfileForm.value).subscribe(
      (res) => {
        console.log('res', res)
        this.validate = !this.validate;
        this.expertProfileForm.reset();
        this.getExpertProfileList();
      },

      (error) => {
        console.log('error', error)
      }
    )
  }

  edit(item: any) {
    this.selectedId = item._id;
    this.expertProfileForm.patchValue({
      name: item.name,
      backGround: item.backGround,
      experience: item.experience,
      mobileNo: item.mobileNo,
      location: item.location
    });
    this.showSubmitButton = false
  }

  updateExpertProfileList() {
    this.httpService.put('expertProfile/updateExpertProfile', this.selectedId, this.expertProfileForm.value).subscribe(
      (res) => {
        console.log('res', res)
        this.getExpertProfileList();
        this.showSubmitButton = true;
        this.expertProfileForm.reset();
      },

      (error) => {
        console.log('error', error)
      }
    )
  }

  deleteExpertProfileList(i: any) {
    this.httpService.delete('expertProfile/deleteExpertProfileList', i).subscribe(
      (res) => {
        console.log('res', res)
        this.getExpertProfileList();
      },

      (error) => {
        console.log('error', error)
      }
    )
  }

}
