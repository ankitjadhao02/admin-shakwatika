import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigurationRoutingModule } from './configuration-routing.module';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GalleryModule } from 'ng-gallery';
import { BarRatingModule } from 'ngx-bar-rating';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { NgxPrintModule } from 'ngx-print';
import { SharedModule } from 'src/app/shared/shared.module';
import { UsersCardsComponent } from './user/users-cards/users-cards.component';
import { UsersEditComponent } from './user/users-edit/users-edit.component';
import { FarmersDetailsComponent } from './framersDetails/farmers-details/farmers-details.component';
import { ExpertProfileComponent } from './expertProfile/expert-profile/expert-profile.component';
import { ListExpertProfileComponent } from './expertProfile/list-expert-profile/list-expert-profile.component';
import { ListfarmersDetailsComponent } from './framersDetails/listfarmers-details/listfarmers-details.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { AddProductComponent } from './product/add-product/add-product.component';
import { ProductDetailsComponent } from './product/product-details/product-details.component';
import { UserProfileComponent } from './user/users-profile/user-profile/user-profile.component';
import { UsersProfileComponent } from './user/users-profile/users-profile.component';
import { AboutMeComponent } from './user/users-profile/left-content/about-me/about-me.component';
import { LeftContentComponent } from './user/users-profile/left-content/left-content.component';
import { FollowersComponent } from './user/users-profile/left-content/followers/followers.component';
import { LatestPhotosComponent } from './user/users-profile/left-content/latest-photos/latest-photos.component';
import { FollowingsComponent } from './user/users-profile/left-content/followings/followings.component';
import { SinglePostComponent } from './user/users-profile/right-content/single-post/single-post.component';
import { DoubleComponent } from './user/users-profile/right-content/double/double.component';
import { FriendsComponent } from './user/users-profile/left-content/friends/friends.component';
import { RightContentComponent } from './user/users-profile/right-content/right-content.component';
import { RoleComponent } from './role/role.component';
import { CategoryComponent } from './category/category.component';
import { AddressComponent } from './address/address.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter'; 
import { NgxPaginationModule } from 'ngx-pagination';
import { ComplaintComponent } from './complaint/complaint.component';
import { FeedBackComponent } from './feed-back/feed-back.component';
import { ChannelPartnerComponent } from './channel-partner/channel-partner.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { OrderComponent } from './order/order.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';


@NgModule({
  declarations: [
    ProductListComponent,
    AddProductComponent,
    ProductDetailsComponent,
    UsersProfileComponent,
    UsersEditComponent,
    UsersCardsComponent,
    UserProfileComponent,
    AboutMeComponent,
    LeftContentComponent,
    RightContentComponent,
    FollowersComponent,
    LatestPhotosComponent,
    FollowingsComponent,
    SinglePostComponent,
    DoubleComponent,
    FriendsComponent,
    FarmersDetailsComponent,
    ExpertProfileComponent,
    ListExpertProfileComponent,
    ListfarmersDetailsComponent,
    RoleComponent,
    CategoryComponent,
    AddressComponent,
    ComplaintComponent,
    FeedBackComponent,
    ChannelPartnerComponent,
    OrderComponent,
    CustomerDetailsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ConfigurationRoutingModule,
    SharedModule,
    NgxSliderModule,
    GalleryModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxPrintModule,
    BarRatingModule,
    NgxDropzoneModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    PaginationModule.forRoot()
  ],
  exports: [
    UserProfileComponent,
    FollowersComponent,
    LatestPhotosComponent,
    FollowingsComponent,
    LeftContentComponent,
    RightContentComponent,
    FriendsComponent
  ]
})
export class ConfigurationModule { }
