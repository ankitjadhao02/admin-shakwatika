import { Component, HostListener } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader/loader.service';

@Component({
  selector: 'app-feed-back',
  templateUrl: './feed-back.component.html',
  styleUrls: ['./feed-back.component.scss']
})
export class FeedBackComponent {
  feedBackdetails: any;
  searchText: any = "";
  currentPage : any = 1;
  itemsPerPage = 5;

  constructor(
    private httpService: HttpService,
    private loaderService: LoaderService,
    private router: Router,
    private modalService: BsModalService,
    private fb: FormBuilder, ) {
    /*   this.qParams = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
      console.log('this.qParams', this.qParams) */
  }

  ngOnInit(): void {
    this.getAllFeedBacks();
  }

  @HostListener('window:resize')
  adjustTableHeight() {
    const windowHeight = window.innerHeight;
    const container = document.querySelector('.table-container') as HTMLElement;
    const containerRect = container.getBoundingClientRect();
    const containerTopOffset = containerRect.top;
    const containerBottomOffset = containerRect.bottom;

    const availableHeight =
      windowHeight - containerTopOffset - containerBottomOffset;
    container.style.height = `${availableHeight}px`;
  }

  getAllFeedBacks() {
    // this.loaderService.showLoader()
    this.httpService.get('getAllFeedBacks').subscribe(
      (res) => {
        this.feedBackdetails = res.data;
        console.log('this.feedBackdetails', this.feedBackdetails)
        // this.loaderService.hideLoader();
      },
      (error) => {
        console.log('error', error);
        // this.loaderService.hideLoader();
      }
    )
  }

  getData(): any[] {
    // Replace this with your actual data retrieval logic
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    return this.feedBackdetails?.slice(startIndex, endIndex);
  }

  get totalPages(): number {
    return Math.ceil(this.feedBackdetails?.length / this.itemsPerPage);
  }

  get pages(): number[] {
    return Array(this.totalPages)
      .fill(0)
      .map((_, i) => i + 1);
  }

  updateCurrentPage(page: number): void {
    if (page >= 1 && page <= this.totalPages) {
      this.currentPage = page;
    }
  }

  getPages(): number[] {
    const totalPages = this.getTotalPages();
    return Array(totalPages)
      .fill(0)
      .map((_, index) => index + 1);
  }

  getTotalPages(): number {
    return Math.ceil(this.feedBackdetails?.length / this.itemsPerPage);
  }
}
