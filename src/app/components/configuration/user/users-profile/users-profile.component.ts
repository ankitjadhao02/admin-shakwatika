import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users-profile',
  templateUrl: './users-profile.component.html',
  styleUrls: ['./users-profile.component.scss']
})
export class UsersProfileComponent implements OnInit {
  public isProfile = false;
  qParams: any;
  constructor(private router: Router) {
    this.qParams = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
    console.log('this.qParams', this.qParams)
  }

  ngOnInit(): void {
  }

}
