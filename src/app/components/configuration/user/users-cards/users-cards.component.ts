import { Component, OnInit } from '@angular/core';
import * as userData from '../../../../shared/data/user/user'
import { HttpService } from 'src/app/shared/services/http/http.service';
import { NavigationExtras, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoaderService } from 'src/app/shared/services/loader/loader.service';
@Component({
  selector: 'app-users-cards',
  templateUrl: './users-cards.component.html',
  styleUrls: ['./users-cards.component.scss']
})
export class UsersCardsComponent implements OnInit {
  public validate = false;
  public tooltipValidation = false;
  userForm: FormGroup;
  expertProfileData: any;
  showSubmitButton = true;
  selectedId: any;
  params: any;
  searchText: any = "";

  pagination = {
    limit: 5,
    currentPage: 1,
    totalCount: 0,
    hasNextPage: false,
    hasPrevPage: false,
  }
  qParams: any;
  userDetials: any;
  singleUserId: any;
  reciveduser: any;


  constructor(
    private httpService: HttpService, 
    private fb: FormBuilder, 
    private loaderService: LoaderService, 
    private router: Router
    ) 
    {
      // this.reciveduser = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
      // console.log(this.reciveduser);
    this.expertForm();
  }

  ngOnInit(): void {
    this.reciveduser = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
      console.log(this.reciveduser);
    // this.getAllUserList();
  }

  expertForm() {
    this.userForm = this.fb.group({
      firstName: [this.qParams ? this.qParams.firstName : '', Validators.required],
      lastName: [this.qParams ? this.qParams.lastName : '', Validators.required],
      email: [this.qParams ? this.qParams.email : '', Validators.required],
      mobileNo: '91' + [ this.qParams ? this.qParams.mobileNo :'', Validators.required],
      password:  [ this.qParams ? this.qParams.password :'', Validators.required],
      profileImage: [ this.qParams ? this.qParams.profileImage :'', Validators.required]
    });
  }

  public submit() {
    this.validate = !this.validate;
    if (this.userForm.invalid) {
      return;
    }
    this.addUser();
  }

  get filteredItems() {
    return this.expertProfileData?.filter((item: any) => item.name.includes(this.searchText) || item.location.includes(this.searchText));
  }

  getAllUserList() {
    // this.loaderService.showLoader()
    this.httpService.get('getAllUserList').subscribe(
      (res) => {
        this.userDetials = res;
        console.log('this.userDetials', this.userDetials)
        this.pagination = res.pagination;
        // this.loaderService.hideLoader();
      },

      (error) => {
        console.log('error', error);
        // this.loaderService.hideLoader();
      }
    )
  }

  addUser() {
    console.log('this.userForm.value', this.userForm.value)
    this.httpService.post('createUser', this.userForm.value).subscribe(
      (res) => {
        console.log('res', res)
        this.validate = !this.validate;
        this.userForm.reset();
        this.getAllUserList();
      },

      (error) => {
        console.log('error', error)
      }
    )
  }

  // edit(item: any) {
  //   console.log('item', item)
  //   let objToSend: NavigationExtras = {
  //     queryParams: item,
  //     skipLocationChange: false,
  //     fragment: 'top'
  //   };
  //   this.singleUserId = this.userDetials.find((x: any) => x.UserId == item);
  //   console.log('this.singleUserId', this.singleUserId)
  //   this.patchSignleValueTask(this.singleUserId);
  //   this.router.navigate(['/configuration/users-cards'], { state: objToSend });
  // }
  // patchSignleValueTask(singleUserId: any) {
  //   console.log('singleUserId', singleUserId)
  //   this.userForm.get('userId')?.patchValue(singleUserId.userId);
  //   this.userForm.get('role')?.patchValue(singleUserId.role);
  //   this.userForm.get('mobileNo')?.patchValue(singleUserId.mobileNo);
  //   this.userForm.get('firstName')?.patchValue(singleUserId.firstName);
  //   this.userForm.get('lastName')?.patchValue(singleUserId.lastName);
  //   this.userForm.get('email')?.patchValue(singleUserId.catOfferPrice);
  //   this.userForm.get('password')?.patchValue(singleUserId.password);
  // }
  // edit(item: any) {
  //   this.selectedId = item._id;
  //   this.userForm.patchValue({
  //     userId: item.userId,
  //     id: item.id,
  //     title: item.title,
  //     body: item.body,
  //   });
  //   this.showSubmitButton = false
  // }

  updateExpertProfileList() {
    this.httpService.put('posts', this.selectedId, this.userForm.value).subscribe(
      (res) => {
        console.log('res', res)
        this.getAllUserList();
        this.showSubmitButton = true;
        this.userForm.reset();
      },

      (error) => {
        console.log('error', error)
      }
    )
  }

  deleteUserById(i: any) {
    this.httpService.delete('deleteUserById', i).subscribe(
      (res) => {
        console.log('res', res)
        this.getAllUserList();
      },

      (error) => {
        console.log('error', error)
      }
    )
  }




  // data
  // public userCards = userData.userCards;
  userData1: any;

  // constructor(private httpService: HttpService, private router: Router) {

  // }

  // ngOnInit(): void {
  //   this.getUserList();
  // }

  getUserList() {
    this.httpService.get('users/getUsersList').subscribe(
      (res) => {
        console.log('res', res)
        this.userData1 = res.data;
      },

      (error) => {
        console.log('error', error)
      }
    )
  }

  goToUserProfile(user: any) {
    let objToSend: NavigationExtras = {
      queryParams: user,
      skipLocationChange: false,
      fragment: 'top'
    };

    this.router.navigate(['/configuration/users-profile'], { state: objToSend });
  }

}
