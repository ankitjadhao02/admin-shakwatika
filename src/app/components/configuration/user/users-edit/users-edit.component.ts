import { HttpClient } from '@angular/common/http';
import { Component, HostListener, OnInit, QueryList, TemplateRef, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader/loader.service';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { NgbdSortableHeader, SortEvent } from 'src/app/shared/directives/sortable.directive';
import { Observable } from 'rxjs';
import { Table } from 'src/app/shared/interface/table';
import { TableService } from 'src/app/shared/services/tables/tables.service';


@Component({
  selector: 'app-users-edit',
  templateUrl: './users-edit.component.html',
  styleUrls: ['./users-edit.component.scss']
})
export class UsersEditComponent implements OnInit {
  @ViewChildren(NgbdSortableHeader) headers!: QueryList<NgbdSortableHeader>;
  basicTable$: Observable<Table[]>;
  total$: Observable<number>;
  searchText: any = "";
  userForm: FormGroup;
  selectedId: any;
  roleDetails: any;
  userDetials: any = [];
  qParams: any;
  userId: string | null;
  singleCategoryId: any;
  singleUserId: any;
  modalRef?: BsModalRef;
  uploadedImageUrl: Object;
  editedUserProfileImage: any;
  selectedRoleId: any;
  isEditing: boolean = false;
  profileImageFile: File | null = null;
  profileImageSrc: string | null = null;
  currentPage : any = 1;
  itemsPerPage = 5;
  roleId: any;


  constructor(
    private httpService: HttpService,
    private loaderService: LoaderService,
    private router: Router,
    private modalService: BsModalService,
    private fb: FormBuilder,
    private http: HttpClient,
    private toastr: ToastrService,
    public service: TableService

  ) {
    /*   this.qParams = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
      console.log('this.qParams', this.qParams) */
    this.expertForm();
    this.basicTable$ = service.basicTable$;
    this.total$ = service.total$;
  }

  ngOnInit(): void {
    this.userId = this.httpService.getUserId();
    this.getAllUserList();
    this.getAllRoleList();
  }

  openModal(template: TemplateRef<any>, isEditing: boolean) {
    this.modalRef = this.modalService.show(template);
    this.clear();
    console.log('this.modalRef', this.modalRef);
    this.isEditing = isEditing;

  }

  modalClose() {
    this.modalRef?.hide();
    // location.reload();
  }

  @HostListener('window:resize')
  adjustTableHeight() {
    const windowHeight = window.innerHeight;
    const container = document.querySelector('.table-container') as HTMLElement;
    const containerRect = container.getBoundingClientRect();
    const containerTopOffset = containerRect.top;
    const containerBottomOffset = containerRect.bottom;
    const availableHeight =
      windowHeight - containerTopOffset - containerBottomOffset;
    container.style.height = `${availableHeight}px`;
  }

  get filteredItems() {
    return this.userDetials?.filter((item: any) => item.name.includes(this.searchText) || item.location.includes(this.searchText));
  }

  expertForm() {
    this.userForm = this.fb.group({
      roleId: new FormControl(),
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      mobileNo: ['', Validators.required],
      profileImage: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach((header) => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });
    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }

  changeRoleId() {
    if (this.userForm) {
      const selectedroleId = this.userForm.get('roleId')?.value;
      console.log('Selected role ID:', selectedroleId);
    }
  }

  getAllRoleList() {
    this.httpService.get('getAllRoleList').subscribe(
      (res: any) => {
        this.roleDetails = res.data;
        console.log('roleDetails', this.roleDetails);
        if (this.roleDetails.length > 0) {
          this.roleId = this.roleDetails[0].roleId;
          console.log('this.roleId', this.roleId);
        }
      },
      (error: any) => {
        console.log('error', error);
      }
    )
  }

  getAllUserList() {
    this.httpService.get('getAllUserList').subscribe(
      (res) => {
        this.userDetials = res.data;
        console.log('this.userDetials', this.userDetials)
      },
      (error) => {
        console.log('error', error);
      }
    )
  }

  uploadUserImage(event: any): void {
    const files = event.target.files;
    if (files && files.length > 0) {
      const formData = new FormData();
      for (const file of files) {
        formData.append('imageFile', file, file.name);
      }
      this.http.post('https://shakvatika.chargurev.com/uploadUserProfileImage', formData)
        .subscribe(
          (response) => {
            console.log('Upload successful:', response);
            this.uploadedImageUrl = response;
            this.profileImageFile = files[0]; // Store the selected file
            this.profileImageSrc = response as string;
          },
          (error) => {
            console.error('Upload error:', error);
          }
        );
    }
  }

  addUser() {
    let sendData = {
      roleId: this.userForm.value.roleId,
      firstName: this.userForm.value.firstName,
      lastName: this.userForm.value.lastName,
      email: this.userForm.value.email,
      mobileNo: this.userForm.value.mobileNo,
      password: this.userForm.value.password,
      profileImage: this.uploadedImageUrl,
    }
    this.httpService.post('createUser', sendData).subscribe(
      (res) => {
           this.toastr.success('Add User Successfully');
        this.clear();
        console.log('res', res)
        this.userForm.reset();
        this.getAllUserList();
        // location.reload();
      },

      (error) => {
        console.log('error', error);
           this.toastr.error(error?.error?.messages?.error);
          //  this.toastr.error("abc");
          //  const errorMessage = error?.messages?.error;
          //  if (errorMessage) {
          //    this.toastr.error(errorMessage);
          //  } else {
          //    this.toastr.error("An error occurred."); // Fallback message
          //  }
      }
    )
  }

  openEditModal(id: any, template: any,isEditing: boolean) {
    console.log('id', id)
    this.singleUserId = this.userDetials.find((x: any) => x.userId == id);
    console.log('this.singleUserId', this.singleUserId);
    this.selectedRoleId = this.singleUserId?.role?.roleId;
    console.log('this.selectedRoleId', this.selectedRoleId)
    this.isEditing = isEditing;
    this.openModal(template ,isEditing);
    this.editedUserProfileImage = this.singleUserId.profileImage;
    this.patchSignleValueTask(this.singleUserId);
  }

  edit() {
    const updatedprofileImage = this.uploadedImageUrl || this.singleUserId.profileImage;

    const sendData = {
      roleId: this.userForm.value.roleId,
      firstName: this.userForm.value.firstName,
      lastName: this.userForm.value.lastName,
      email: this.userForm.value.email,
      mobileNo: this.userForm.value.mobileNo,
      password: this.userForm.value.password,
      profileImage: updatedprofileImage,
    };

    this.httpService.put('editUserByUserId', this.singleUserId.userId, sendData)
      .subscribe({
        next: (response) => {
          console.log('response', response);
          this.toastr.success('Update user Successfully');

          this.getAllUserList();
          this.userForm.reset();
          this.modalRef?.hide();
          // location.reload();
        },
        error: (err) => {
          console.log('err', err);
          this.toastr.error(err?.error?.messages.error);
          // const errorMessage = err?.messages?.error;
          // if (errorMessage) {
          //   this.toastr.error(errorMessage);
          // } else {
          //   this.toastr.error("An error occurred."); // Fallback message
          // }
        },
        complete: () => {
          console.info('complete.!!');
        },
      });
  }

  patchSignleValueTask(singleUserId: any) {
    console.log('singleUserId', singleUserId)
    this.selectedRoleId = this.singleUserId?.role?.roleId;
    console.log('this.selectedRoleId', this.selectedRoleId);
    this.userForm.get('firstName')?.patchValue(singleUserId.firstName);
    this.userForm.get('lastName')?.patchValue(singleUserId.lastName);
    this.userForm.get('mobileNo')?.patchValue(singleUserId.mobileNo);
    this.userForm.get('email')?.patchValue(singleUserId.email);
    this.userForm.get('profileImage')?.patchValue(singleUserId.profileImage);
    this.userForm.get('password')?.patchValue(singleUserId.password);
    this.profileImageSrc = singleUserId.profileImage;
  }

  submitForm() {
    if (this.userForm.valid) {
      const formData = this.userForm.value;
      console.log(formData);
    } else {
      // Handle form validation errors
    }
  }

  deleteUserById(id: any) {
    Swal.fire({
      title: 'Delete',
      text: "Are you sure?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it'
    }).then((result) => {
      if (result.isConfirmed) {
        this.httpService.delete('deleteUserById', id).subscribe(
          (res: any) => {
            console.log('res', res);
            this.getAllUserList();
          },
          (error: any) => {
            console.log('error', error);
          }
        );
      }
    });
  }

  /* deleteUserById(i: any) {
    this.httpService.delete('deleteUserById', i).subscribe(
      (res) => {
        console.log('res', res)
        this.getAllUserList();
      },

      (error) => {
        console.log('error', error)
      }
    )
  } */

  clear() {
    this.userForm.reset();
    this.profileImageFile = null; // Clear the image URL
    this.profileImageSrc = null; // Clear the image file
    this.selectedRoleId = null; 
  }

  getData(): any[] {
    // Replace this with your actual data retrieval logic
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    return this.userDetials?.slice(startIndex, endIndex);
  }

  get totalPages(): number {
    return Math.ceil(this.userDetials?.length / this.itemsPerPage);
  }

  get pages(): number[] {
    return Array(this.totalPages)
      .fill(0)
      .map((_, i) => i + 1);
  }

  updateCurrentPage(page: number): void {
    if (page >= 1 && page <= this.totalPages) {
      this.currentPage = page;
    }
  }

  getPages(): number[] {
    const totalPages = this.getTotalPages();
    return Array(totalPages)
      .fill(0)
      .map((_, index) => index + 1);
  }

  getTotalPages(): number {
    return Math.ceil(this.userDetials?.length / this.itemsPerPage);
  }

}
