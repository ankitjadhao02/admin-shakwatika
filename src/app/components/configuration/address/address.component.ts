import { Component, HostListener, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators , FormControl} from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader/loader.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent {
  searchText: any = "";
  adressForm: FormGroup;
  isEditing: boolean = false;
  selectedId: any;
  modalRef?: BsModalRef;
  AddressList: any = [];
  singleaddressId: any;
  userId: string | null;
  userDetials: any;
  currentPage : any = 1;
  itemsPerPage = 5;

  constructor(private httpService: HttpService, private loaderService: LoaderService, private router: Router, private fb: FormBuilder, private modalService: BsModalService,   private toastr: ToastrService,) {
    this.adressForm = this.fb.group({
      userId: new FormControl(),
      fullName: ['', Validators.required],
      mobileNo:["", [Validators.required, Validators.pattern('^[0-9]{10}$')]],
      addressType: ['', Validators.required],
      address: ['', Validators.required],
      pincode: ['', Validators.required],
      locality: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      defaultAddress: ['', Validators.required],
    });

  }

  ngOnInit(): void {
    this.getAddressList();
    this.getAllUserList(); 
  }

  @HostListener('window:resize')
  adjustTableHeight() {
    const windowHeight = window.innerHeight;
    const container = document.querySelector('.table-container') as HTMLElement;
    const containerRect = container.getBoundingClientRect();
    const containerTopOffset = containerRect.top;
    const containerBottomOffset = containerRect.bottom;

    const availableHeight =
      windowHeight - containerTopOffset - containerBottomOffset;
    container.style.height = `${availableHeight}px`;
  }

  openModal(template: TemplateRef<any>, isEditing: boolean) {
    this.modalRef = this.modalService.show(template);
    this.clear();
    console.log('this.modalRef', this.modalRef)
    this.isEditing = isEditing;
  }

  modalClose() {
    this.modalRef?.hide();
    // location.reload();
  }

  submit() {
    if (this.adressForm.valid) {
      const formData = this.adressForm.value;
      console.log(formData);
    } else {
      // Handle form validation errors
    }
  }

  getAllUserList() {
    this.httpService.get('getAllUserList').subscribe(
      (res) => {
        this.userDetials = res.data;
        console.log('this.userDetials', this.userDetials)
       
      },
      (error) => {
        console.log('error', error);
      }
    )
  }

  getAddressList() {
    this.httpService.get('getAddressList').subscribe(
      (res) => {
        console.log('res', res)
        this.AddressList = res.data;
      },

      (error) => {
        console.log('error', error);
      }
    )
  }
  changeuserId(){
    if (this.adressForm) {
      const userId = this.adressForm.get('userId')?.value;
      console.log('Selected userId ID:', userId);
    }
  }
  createAddress() {
    let sendData = {
      userId: this.adressForm.value.userId,
      fullName: this.adressForm.value.fullName,
      mobileNo: '91' + this.adressForm.value.mobileNo,
      address: this.adressForm.value.address,
      pincode: this.adressForm.value.pincode,
      locality: this.adressForm.value.locality,
      state: this.adressForm.value.state,
      city: this.adressForm.value.city,
      defaultAddress: this.adressForm.value.defaultAddress,
      addressType: this.adressForm.value.addressType,
    }    // console.log('this.adressForm.value', this.adressForm.value)
    this.httpService.post('createAddress', sendData).subscribe(
      (res) => {
        this.clear();
        console.log('res', res);
        this.toastr.success('Add Adress Successfully');
        this.adressForm.reset();
        this.getAddressList();
      },

      (error) => {
        console.log('error', error);
        this.toastr.error(error?.error?.messages.error);
        // const errorMessage = error?.messages?.error;
        // if (errorMessage) {
        //   this.toastr.error(errorMessage);
        // } else {
        //   this.toastr.error("An error occurred."); // Fallback message
        // }
      }
    )
  }

  openEditModal(id: any, template: any,  isEditing: boolean) {
    console.log('id', id)
    this.singleaddressId = this.AddressList.find((x: any) => x.addressId == id);
    console.log('this.singleaddressId', this.singleaddressId);
    this.isEditing = isEditing;
    this.openModal(template,isEditing);
    this.patchSignleValueTask(this.singleaddressId);  

  }

  UpdateAddressByAddressId() {

    const sendData = {
      userId: this.adressForm.value.userId,
      fullName: this.adressForm.value.fullName,
      mobileNo:  this.adressForm.value.mobileNo,
      address: this.adressForm.value.address,
      pincode: this.adressForm.value.pincode,
      locality: this.adressForm.value.locality,
      city: this.adressForm.value.city,
      state: this.adressForm.value.state,
      defaultAddress:  this.adressForm.value.defaultAddress,
      addressType: this.adressForm.value.addressType, // Include the updated addressType value
    };
  
    this.httpService.put('UpdateAddressByAddressId', this.singleaddressId.addressId, sendData).subscribe({
        next: (response) => {
          console.log('response', response);
          this.toastr.success('Update Address Successfully');
          this.getAddressList();
          this.adressForm.reset();
          this.modalRef?.hide();
        },
        error: (err) => {
          console.log('err', err);
          this.toastr.error(err?.error?.messages.error);
          // const errorMessage = err?.messages?.error;
          // if (errorMessage) {
          //   this.toastr.error(errorMessage);
          // } else {
          //   this.toastr.error("An error occurred."); // Fallback message
          // }
        },
        complete: () => {
          console.info('complete.!!');
        },
      });
  }

  patchSignleValueTask(singleaddressId: any) {
    console.log('singleaddressId', singleaddressId)
    this.adressForm.get('userId')?.patchValue(singleaddressId.userId);
    this.adressForm.get('fullName')?.patchValue(singleaddressId.fullName);
    this.adressForm.get('mobileNo')?.patchValue(singleaddressId.mobileNo);
    this.adressForm.get('addressType')?.patchValue(singleaddressId.addressType);
    this.adressForm.get('address')?.patchValue(singleaddressId.address);
    this.adressForm.get('pincode')?.patchValue(singleaddressId.pincode);
    this.adressForm.get('locality')?.patchValue(singleaddressId.locality);
    this.adressForm.get('state')?.patchValue(singleaddressId.state);
    this.adressForm.get('city')?.patchValue(singleaddressId.city);
    this.adressForm.get('defaultAddress')?.patchValue(singleaddressId.defaultAddress);
  }

  deleteAddressByAddressId(id: any) {
    Swal.fire({
      title: 'Delete',
      text: "Are you sure?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it'
    }).then((result) => {
      if (result.isConfirmed) {
        this.httpService.delete('deleteAddressByAddressId', id).subscribe(
          (res: any) => {
            console.log('res', res);
            this.getAddressList();
          },
          (error: any) => {
            console.log('error', error);
          }
        );
      }
    });
  }

/*   deleteAddressByAddressId(id: any) {
    this.httpService.delete('deleteAddressByAddressId', id).subscribe(
      (res: any) => {
        console.log('res', res)
        this.getAddressList();
      },
      (error: any) => {
        console.log('error', error)
      }
    )
  }
 */

  clear() {
    this.adressForm.reset();
  }

  getData(): any[] {
    // Replace this with your actual data retrieval logic
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    return this.AddressList?.slice(startIndex, endIndex);
  }

  get totalPages(): number {
    return Math.ceil(this.AddressList?.length / this.itemsPerPage);
  }

  get pages(): number[] {
    return Array(this.totalPages)
      .fill(0)
      .map((_, i) => i + 1);
  }

  updateCurrentPage(page: number): void {
    if (page >= 1 && page <= this.totalPages) {
      this.currentPage = page;
    }
  }

  getPages(): number[] {
    const totalPages = this.getTotalPages();
    return Array(totalPages)
      .fill(0)
      .map((_, index) => index + 1);
  }

  getTotalPages(): number {
    return Math.ceil(this.AddressList?.length / this.itemsPerPage);
  }

}
