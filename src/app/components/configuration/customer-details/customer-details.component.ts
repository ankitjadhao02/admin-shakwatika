import { Component } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http/http.service';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.scss']
})
export class CustomerDetailsComponent {
  searchText: any = "";
  currentPage : any = 1;
  itemsPerPage = 5;
  customerDetails: any;

  constructor(private httpService : HttpService) { }

  ngOnInit(): void { 
    // this.getCustomerDetails();
  }


  getCustomerDetails(){
    this.httpService.get('getCustomerDetails').subscribe(
      (res: any) => {
        this.customerDetails = res;
        console.log('customerDetails', this.customerDetails);
      },
      (error: any) => {
        console.log('error', error);
      }
    )
  }

  getData(): any[] {
    // Replace this with your actual data retrieval logic
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    return this.customerDetails?.slice(startIndex, endIndex);
  }

  get totalPages(): number {
    return Math.ceil(this.customerDetails?.length / this.itemsPerPage);
  }

  get pages(): number[] {
    return Array(this.totalPages)
      .fill(0)
      .map((_, i) => i + 1);
  }

  updateCurrentPage(page: number): void {
    if (page >= 1 && page <= this.totalPages) {
      this.currentPage = page;
    }
  }

  getPages(): number[] {
    const totalPages = this.getTotalPages();
    return Array(totalPages)
      .fill(0)
      .map((_, index) => index + 1);
  }

  getTotalPages(): number {
    return Math.ceil(this.customerDetails?.length / this.itemsPerPage);
  }

}
