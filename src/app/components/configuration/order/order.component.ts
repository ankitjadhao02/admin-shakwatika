import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader/loader.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent {
  searchText: any = "";

  pagination = {
    limit: 5,
    currentPage: 1,
    totalCount: 0,
    hasNextPage: false,
    hasPrevPage: false,
  }
  expertProfileData: any;
  showSubmitButton: boolean;
  selectedId: any;
  orderDetails: any;
  currentPage : any = 1;
  itemsPerPage = 10;

  constructor(
    private httpService: HttpService,
    private loaderService: LoaderService,
    private router: Router) {

  }

  ngOnInit(): void {
    this.getOrderList();
  }

  

  get filteredItems() {
    return this.expertProfileData?.filter((item: any) => item.name.includes(this.searchText) || item.location.includes(this.searchText));
  }

  getOrderList() {
    // this.loaderService.showLoader()
    this.httpService.get('getOrderList').subscribe(
      (res :any ) => {
        // console.log('res', res)
        this.orderDetails = res.data;
        console.log('orderDetails', this.orderDetails)
        // this.pagination = res.pagination;
        // this.loaderService.hideLoader();
      },

      (error : any) => {
        console.log('error', error);
        // this.loaderService.hideLoader();
      }
    )
  }

  edit(item: any) {
    console.log('item', item)
    let objToSend: NavigationExtras = {
      queryParams: item,
      skipLocationChange: false,
      fragment: 'top'
    };
    this.router.navigate(['/configuration/demo'], { state: objToSend });
  }

  deleteExpertProfileList(i: any) {
    this.httpService.delete('posts', i).subscribe(
      (res : any) => {
        console.log('res', res)
      },

      (error : any) => {
        console.log('error', error)
      }
    )
  }

  getData(): any[] {
    // Replace this with your actual data retrieval logic
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    return this.orderDetails?.slice(startIndex, endIndex);
  }

  get totalPages(): number {
    return Math.ceil(this.orderDetails?.length / this.itemsPerPage);
  }

  get pages(): number[] {
    return Array(this.totalPages)
      .fill(0)
      .map((_, i) => i + 1);
  }

  updateCurrentPage(page: number): void {
    if (page >= 1 && page <= this.totalPages) {
      this.currentPage = page;
    }
  }

  getPages(): number[] {
    const totalPages = this.getTotalPages();
    return Array(totalPages)
      .fill(0)
      .map((_, index) => index + 1);
  }

  getTotalPages(): number {
    return Math.ceil(this.orderDetails?.length / this.itemsPerPage);
  }
}
