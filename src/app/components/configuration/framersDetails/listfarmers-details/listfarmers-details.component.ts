import { Component, HostListener, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader/loader.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listfarmers-details',
  templateUrl: './listfarmers-details.component.html',
  styleUrls: ['./listfarmers-details.component.scss']
})
export class ListfarmersDetailsComponent {
  searchText: any = "";
  userForm: FormGroup;
  pagination = {
    limit: 5,
    currentPage: 1,
    totalCount: 0,
    hasNextPage: false,
    hasPrevPage: false,
  }
  expertProfileData: any;
  isEditing: boolean = false;
  selectedId: any;
  roleDetails: any;
  farmerDetials: any=[];
  singleFarmerId: any;
  qParams: any;
  userId: string | null;
  singleCategoryId: any;
  currentPage : any = 1;
  itemsPerPage = 5;

  reciveduser: any;
  modalRef?: BsModalRef;

  constructor(
    private httpService: HttpService,
    private loaderService: LoaderService,
    private router: Router,
    private modalService: BsModalService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    ) {
    /*   this.qParams = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
      console.log('this.qParams', this.qParams) */
      this.expertForm();
  }

  ngOnInit(): void {
    this.userId = this.httpService.getUserId();
    this.getFarmerDetails();
  
  }
  openModal(template: TemplateRef<any>,isEditing: boolean) {
    this.modalRef = this.modalService.show(template);
    this.clear();
    console.log('this.modalRef', this.modalRef)
    this.isEditing = isEditing;
  }

  @HostListener('window:resize')
  adjustTableHeight() {
    const windowHeight = window.innerHeight;
    const container = document.querySelector('.table-container') as HTMLElement;
    const containerRect = container.getBoundingClientRect();
    const containerTopOffset = containerRect.top;
    const containerBottomOffset = containerRect.bottom;

    const availableHeight =
      windowHeight - containerTopOffset - containerBottomOffset;
    container.style.height = `${availableHeight}px`;
  }

  

  get filteredItems() {
    return this.farmerDetials?.filter((item: any) => item.name.includes(this.searchText) || item.location.includes(this.searchText));
  }
  expertForm() {
    this.userForm = this.fb.group({
      fullName: ['', Validators.required],
      landDetails: [ '', Validators.required],
      location: ['', Validators.required],
      googleMap: ['', Validators.required],
      dimensions: ['', Validators.required],
      farmSupervisorName:  [ '', Validators.required],
      contactNo:  [ '', Validators.required],
    });
  }

  getFarmerDetails() {
    this.httpService.get('getFarmerDetails').subscribe(
      (res) => {
        this.farmerDetials = res.data;
        console.log('this.farmerDetials', this.farmerDetials)
      },
      (error) => {
        console.log('error', error);
      }
    )
  }

  createFarmerDetailsprofile() {
    console.log('this.userForm.value', this.userForm.value)
    this.httpService.post('createFarmerDetailsprofile', this.userForm.value).subscribe(
      (res) => {
        this.toastr.success('Add Farmer Successfully');
        this.clear();
        console.log('res', res)
        this.userForm.reset();
        this.getFarmerDetails();
      },

      (error) => {
        console.log('error', error);
        this.toastr.error(error?.error?.messages.error);
        // const errorMessage = error?.messages?.error;
        // if (errorMessage) {
        //   this.toastr.error(errorMessage);
        // } else {
        //   this.toastr.error("An error occurred."); // Fallback message
        // }
      }
    )
  }

  openEditModal(id: any, template: any , isEditing: boolean) {
    console.log('id', id)
    // this.isEditCLicked = true;
    this.singleFarmerId = this.farmerDetials.find((x: any) => x.id == id);
    console.log('this.singleFarmerId', this.singleFarmerId)
    this.isEditing = isEditing;
    this.openModal(template ,isEditing);
    this.patchSignleValueTask(this.singleFarmerId);
  }

  updateDetailstProfileById() {
    this.httpService
      .put(
        'updateDetailstProfileById',
        this.singleFarmerId.id,
        this.userForm.value
      )
      .subscribe({
        next: (response) => {
          console.log('response', response);
          this.toastr.success('Add Farmer Successfully');
          this.getFarmerDetails();
          this.userForm.reset();
          this.modalRef?.hide();
        },
        error: (err) => {
          console.log('err', err);
          this.toastr.error(err?.error?.messages.error);
          // const errorMessage = err?.messages?.error;
          // if (errorMessage) {
          //   this.toastr.error(errorMessage);
          // } else {
          //   this.toastr.error("An error occurred."); // Fallback message
          // }
        },
        complete: () => {
          console.info('complete.!!');
        },
      });
  }


  patchSignleValueTask(singleFarmerId: any) {
    console.log('singleFarmerId', singleFarmerId)
    // this.userForm.get('userId')?.patchValue(singleFarmerId.userId);
    this.userForm.get('fullName')?.patchValue(singleFarmerId.fullName);
    this.userForm.get('landDetails')?.patchValue(singleFarmerId.landDetails);
    this.userForm.get('location')?.patchValue(singleFarmerId.location);
    this.userForm.get('googleMap')?.patchValue(singleFarmerId.googleMap);
    this.userForm.get('dimensions')?.patchValue(singleFarmerId.dimensions);
    this.userForm.get('farmSupervisorName')?.patchValue(singleFarmerId.farmSupervisorName);
    this.userForm.get('contactNo')?.patchValue(singleFarmerId.contactNo);
    // this.userForm.get('role')?.patchValue(singleFarmerId.role);
  }


 
  submitForm() {
    if (this.userForm.valid) {
      const formData = this.userForm.value;
      console.log(formData);
    } else {
      // Handle form validation errors
    }
  }
 
  deleteFarmerDetailsById(id: any) {
    Swal.fire({
      title: 'Delete',
      text: "Are you sure?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it'
    }).then((result) => {
      if (result.isConfirmed) {
        this.httpService.delete('deleteFarmerDetailsById', id).subscribe(
          (res: any) => {
            console.log('res', res);
            this.getFarmerDetails();
          },
          (error: any) => {
            console.log('error', error);
          }
        );
      }
    });
  }

 /*  deleteFarmerDetailsById(id: any) {
    this.httpService.delete('deleteFarmerDetailsById', id).subscribe(
      (res : any) => {
        console.log('res', res)
        this.getFarmerDetails();
      },
      (error : any) => {
        console.log('error', error)
      }
    )
  } */

  clear() {
    this.userForm.reset();
  }

  getData(): any[] {
    // Replace this with your actual data retrieval logic
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    return this.farmerDetials?.slice(startIndex, endIndex);
  }

  get totalPages(): number {
    return Math.ceil(this.farmerDetials?.length / this.itemsPerPage);
  }

  get pages(): number[] {
    return Array(this.totalPages)
      .fill(0)
      .map((_, i) => i + 1);
  }

  updateCurrentPage(page: number): void {
    if (page >= 1 && page <= this.totalPages) {
      this.currentPage = page;
    }
  }

  getPages(): number[] {
    const totalPages = this.getTotalPages();
    return Array(totalPages)
      .fill(0)
      .map((_, index) => index + 1);
  }

  getTotalPages(): number {
    return Math.ceil(this.farmerDetials?.length / this.itemsPerPage);
  }
  
}

