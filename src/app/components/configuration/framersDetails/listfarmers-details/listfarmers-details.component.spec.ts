import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListfarmersDetailsComponent } from './listfarmers-details.component';

describe('ListfarmersDetailsComponent', () => {
  let component: ListfarmersDetailsComponent;
  let fixture: ComponentFixture<ListfarmersDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListfarmersDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListfarmersDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
