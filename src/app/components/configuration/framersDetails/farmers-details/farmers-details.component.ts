import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from 'src/app/shared/services/http/http.service';

@Component({
  selector: 'app-farmers-details',
  templateUrl: './farmers-details.component.html',
  styleUrls: ['./farmers-details.component.scss']
})
export class FarmersDetailsComponent {
  public validate = false;
  public tooltipValidation = false;
  FarmersDetailsForm: FormGroup;
  FarmersDetailsData: any;
  showSubmitButton = true;
  selectedId: any;


  constructor(private httpService: HttpService, private fb: FormBuilder) {
    this.FarmersDetailsForm = this.fb.group({
      seventTwelve: ['', Validators.required],
      landDetails: ['', Validators.required],
      location: ['', Validators.required],
      googleMap: ['', Validators.required],
      dimensions: ['', Validators.required],
      farmSupervisorName: ['', Validators.required],
      contactNo: ['', [Validators.required, Validators.pattern(/^\d{10}$/)]]
    });
  }

  ngOnInit(): void {
    this.getFarmersDetailsList();
  }

  public submit() {
    this.validate = !this.validate;
    this.addFarmersDetailsFromData();
    this.FarmersDetailsForm.reset();
    if (this.FarmersDetailsForm.invalid) {
      return;
    }
  }

  getFarmersDetailsList() {
    this.httpService.get('farmersDetail/getFarmersDetailList').subscribe(
      (res: any) => {
        console.log('res', res)
        this.FarmersDetailsData = res.data;
      },

      (error: any) => {
        console.log('error', error)
      }
    )
  }

  addFarmersDetailsFromData() {
    this.httpService.post('farmersDetail/addFarmersDetailFromData', this.FarmersDetailsForm.value).subscribe(
      (res: any) => {
        console.log('res', res)
        this.getFarmersDetailsList();
        this.validate = !this.validate;
      },

      (error: any) => {
        console.log('error', error)
      }
    )
  }

  edit(item: any) {
    this.selectedId = item._id;
    this.FarmersDetailsForm.patchValue({
      seventTwelve: item.seventTwelve,
      landDetails: item.landDetails,
      location: item.location,
      googleMap: item.googleMap,
      dimensions: item.dimensions,
      farmSupervisorName: item.farmSupervisorName,
      contactNo: item.contactNo
    });
    this.showSubmitButton = false
  }

  updateExpertProfileList() {
    this.httpService.put('farmersDetail/updateFarmersDetail', this.selectedId, this.FarmersDetailsForm.value).subscribe(
      (res: any) => {
        console.log('res', res)
        this.getFarmersDetailsList();
        this.showSubmitButton = true;
        this.FarmersDetailsForm.reset();
      },

      (error: any) => {
        console.log('error', error)
      }
    )
  }

  deleteExpertProfileList(i: any) {
    this.httpService.delete('farmersDetail/deleteFarmersDetailList', i).subscribe(
      (res: any) => {
        console.log('res', res)
        this.getFarmersDetailsList();
      },

      (error: any) => {
        console.log('error', error)
      }
    )
  }

}
