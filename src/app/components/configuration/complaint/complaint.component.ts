import { Component, HostListener, QueryList, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ProductListDirective } from 'src/app/shared/directives/product-list.directive';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader/loader.service';

@Component({
  selector: 'app-complaint',
  templateUrl: './complaint.component.html',
  styleUrls: ['./complaint.component.scss']
})
export class ComplaintComponent {

  headers!: QueryList<ProductListDirective>;
  searchText: any = "";
  categoryForm: FormGroup;
  pagination = {
    limit: 5,
    currentPage: 1,
    totalCount: 0,
    hasNextPage: false,
    hasPrevPage: false,
  }
  showSubmitButton: boolean = true;
  selectedId: any;
  roleDetails: any;
  categoryDetails: any=[];
  singleCategoryId: any;
  qParams: any;
  userId: string | null;
  reciveduser: any;
  modalRef?: BsModalRef;
  pagedCategoryDetails: any[] = [];
  Complaints: any;
  currentPage : any = 1;
  itemsPerPage = 5;
 
  constructor(
    private httpService: HttpService,
    private loaderService: LoaderService,
    private router: Router,
    private modalService: BsModalService,
    private fb: FormBuilder, ) {
    /*   this.qParams = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
      console.log('this.qParams', this.qParams) */
      this.expertForm();
  }

  ngOnInit(): void {
    this.userId = this.httpService.getUserId();
    this.getAllComplaints();
  
  }

  // modalClose() {
  //   this.modalRef?.hide();
  //   this.showSubmitButton = false;
  //   location.reload();
  //   console.log('444', this.showSubmitButton);
  // }

  // openModal(template: TemplateRef<any>) {
  //   this.modalRef = this.modalService.show(template);
  //   this.clear();
  //   console.log('this.modalRef', this.modalRef)
  // }

  @HostListener('window:resize')
  adjustTableHeight() {
    const windowHeight = window.innerHeight;
    const container = document.querySelector('.table-container') as HTMLElement;
    const containerRect = container.getBoundingClientRect();
    const containerTopOffset = containerRect.top;
    const containerBottomOffset = containerRect.bottom;

    const availableHeight =
      windowHeight - containerTopOffset - containerBottomOffset;
    container.style.height = `${availableHeight}px`;
  }

  expertForm() {
    this.categoryForm = this.fb.group({
      categoryName: ['', Validators.required],
      description: [ '', Validators.required],
     
    });
  }

  getAllComplaints() {
    // this.loaderService.showLoader()
    this.httpService.get('getAllComplaints').subscribe(
      (res) => {
        this.Complaints = res.data;
        console.log('this.Complaints', this.Complaints)
        // this.loaderService.hideLoader();
      },
      (error) => {
        console.log('error', error);
        // this.loaderService.hideLoader();
      }
    )
  }

  getData(): any[] {
    // Replace this with your actual data retrieval logic
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    return this.Complaints?.slice(startIndex, endIndex);
  }

  get totalPages(): number {
    return Math.ceil(this.Complaints?.length / this.itemsPerPage);
  }

  get pages(): number[] {
    return Array(this.totalPages)
      .fill(0)
      .map((_, i) => i + 1);
  }

  updateCurrentPage(page: number): void {
    if (page >= 1 && page <= this.totalPages) {
      this.currentPage = page;
    }
  }

  getPages(): number[] {
    const totalPages = this.getTotalPages();
    return Array(totalPages)
      .fill(0)
      .map((_, index) => index + 1);
  }

  getTotalPages(): number {
    return Math.ceil(this.Complaints?.length / this.itemsPerPage);
  }
  
  // createCategory() {
  //   console.log('this.categoryForm.value', this.categoryForm.value)
  //   this.httpService.post('createCategory', this.categoryForm.value).subscribe(
  //     (res) => {
  //       this.clear();
  //       console.log('res', res)
  //       this.categoryForm.reset();
  //       this.getAllCategory();
  //     },

  //     (error) => {
  //       console.log('error', error)
  //     }
  //   )
  // }

  // openEditModal(id: any, template: any) {
  //   console.log('id', id)
  //   this.showSubmitButton = false;
  //   this.singleCategoryId = this.categoryDetails.find((x: any) => x.categoryId == id);
  //   console.log('this.singleCategoryId', this.singleCategoryId)
  //   this.openModal(template);
  //   this.patchSignleValueTask(this.singleCategoryId);
  // }

  // updateCategoryByCategoryId() {
  //   this.httpService
  //     .put(
  //       'updateCategoryByCategoryId',
  //       this.singleCategoryId.categoryId ,
  //       this.categoryForm.value
  //     )
  //     .subscribe({
  //       next: (response) => {
  //         console.log('response', response);
  //         this.getAllCategory();
  //         this.categoryForm.reset();
  //         this.modalRef?.hide();
  //         this.showSubmitButton = true;
  //       },
  //       error: (err) => {
  //         console.log('err', err);
  //       },
  //       complete: () => {
  //         console.info('complete.!!');
  //       },
  //     });
  // }


  // patchSignleValueTask(singleCategoryId: any) {
  //   console.log('singleCategoryId', singleCategoryId)
  //   this.categoryForm.get('categoryName')?.patchValue(singleCategoryId.categoryName);
  //   this.categoryForm.get('description')?.patchValue(singleCategoryId.description);
  // }

  // submitForm() {
  //   if (this.categoryForm.valid) {
  //     const formData = this.categoryForm.value;
  //     console.log(formData);
  //   } else {
  //     // Handle form validation errors
  //   }
  // }
 

  // deleteCategoryById(id: any) {
  //   this.httpService.delete('deleteCategoryById', id).subscribe(
  //     (res : any) => {
  //       console.log('res', res)
  //       this.getAllCategory();
  //     },
  //     (error : any) => {
  //       console.log('error', error)
  //     }
  //   )
  // }

  // clear() {
  //   this.categoryForm.reset();
  // }

}
