import { HttpClient } from '@angular/common/http';
import { Component, HostListener, QueryList, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { ProductListDirective } from 'src/app/shared/directives/product-list.directive';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { LoaderService } from 'src/app/shared/services/loader/loader.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-channel-partner',
  templateUrl: './channel-partner.component.html',
  styleUrls: ['./channel-partner.component.scss']
})
export class ChannelPartnerComponent {
  headers!: QueryList<ProductListDirective>;
  searchText: any = "";
  channelForm: FormGroup;
  isEditing: boolean = false;
  roleDetails: any;
  ChanelPartner: any=[];
  singleId: any;
  qParams: any;
  userId: string | null;
  modalRef?: BsModalRef;
  userDetials: any;
  currentPage : any = 1;
  itemsPerPage = 5;
   profileImageFile: File | null = null;
  profileImageSrc: string | null = null;
  uploadedImageUrl: any;
  selectedRoleId: any;
 
  constructor(
    private httpService: HttpService,
    private loaderService: LoaderService,
    private router: Router,
    private modalService: BsModalService,
    private fb: FormBuilder, 
    private http: HttpClient,
    private toastr: ToastrService
    ) {
    /*   this.qParams = this.router.getCurrentNavigation()?.extras.state?.['queryParams'];
      console.log('this.qParams', this.qParams) */
      this.expertForm();
  }

  ngOnInit(): void {
    this.userId = this.httpService.getUserId();
    this.getChanelPartnerProfileList();
    this. getAllUserList();
    this. getAllRoleList();
  
  }

  modalClose() {
    this.modalRef?.hide();
    // location.reload();

  }

  openModal(template: TemplateRef<any>,isEditing: boolean) {
    this.modalRef = this.modalService.show(template);
    this.clear();
    console.log('this.modalRef', this.modalRef);
    this.isEditing = isEditing;
  }

  // @HostListener('window:resize')
  // adjustTableHeight() {
  //   const windowHeight = window.innerHeight;
  //   const container = document.querySelector('.table-container') as HTMLElement;
  //   const containerRect = container.getBoundingClientRect();
  //   const containerTopOffset = containerRect.top;
  //   const containerBottomOffset = containerRect.bottom;

  //   const availableHeight =
  //     windowHeight - containerTopOffset - containerBottomOffset;
  //   container.style.height = `${availableHeight}px`;
  // }

  expertForm() {
    this.channelForm = this.fb.group({
      roleId: new FormControl(),
      firstName: ['', Validators.required],
      lastName : [ '', Validators.required],
      email: ['', Validators.required],
      mobileNo: [ '', Validators.required],
      password: ['', Validators.required],
      profileImage: [ '', Validators.required],
      zipCode: [ '', Validators.required],
     
    });
  }

  changeRoleId() {
    if (this.channelForm) {
      const selectedroleId = this.channelForm.get('roleId')?.value;
      console.log('Selected role ID:', selectedroleId);
    }
  }

  getAllRoleList() {
    this.httpService.get('getAllRoleList').subscribe(
      (res: any) => {
        this.roleDetails = res.data;
        console.log('roleDetails', this.roleDetails);
      },
      (error: any) => {
        console.log('error', error);
      }
    )
  }

  getAllUserList() {
    this.httpService.get('getAllUserList').subscribe(
      (res) => {
        this.userDetials = res.data;
        console.log('this.userDetials', this.userDetials)
       
      },
      (error) => {
        console.log('error', error);
      }
    )
  }

  getChanelPartnerProfileList() {
    // this.loaderService.showLoader()
    this.httpService.get('getChanelPartnerProfileList').subscribe(
      (res) => {
        this.ChanelPartner = res.data;
        console.log('this.ChanelPartner', this.ChanelPartner)
        // this.loaderService.hideLoader();
      },
      (error) => {
        console.log('error', error);
        // this.loaderService.hideLoader();
      }
    )
  }
  
  
  createChannelPartner() {
    let zipCodes = this.channelForm.value.zipCode.split(','); // Split zip codes by ','

    let sendData = {
      firstName: this.channelForm.value.firstName,
      lastName: this.channelForm.value.lastName,
      email: this.channelForm.value.email,
      mobileNo: this.channelForm.value.mobileNo,
      password: this.channelForm.value.password,
      profileImage: this.uploadedImageUrl,
      roleId: this.channelForm.value.roleId,
     zipCode: zipCodes // Use the split zip codes array
    };
    this.httpService.post('createChannelPartner',sendData).subscribe(
      (res) => {
        this.clear();
        console.log('res', res);
        this.toastr.success('Add  Successfully');
        this.channelForm.reset();
        this.getChanelPartnerProfileList();

      },

      (error) => {
        console.log('error', error);
        this.toastr.error(error?.error?.messages.error);
        // const errorMessage = error?.messages?.error;
        // if (errorMessage) {
        //   this.toastr.error(errorMessage);
        // } else {
        //   this.toastr.error("An error occurred."); // Fallback message
        // }
      }
    )
  }

  uploadUserImage(event: any): void {
    const files = event.target.files;
    if (files && files.length > 0) {
      const formData = new FormData();
      for (const file of files) {
        formData.append('imageFile', file, file.name);
      }
      this.http.post('https://shakvatika.chargurev.com/uploadUserProfileImage', formData)
        .subscribe(
          (response) => {
            console.log('Upload successful:', response);
            this.uploadedImageUrl = response;
            this.profileImageFile = files[0]; // Store the selected file
            this.profileImageSrc = response as string;
          },
          (error) => {
            console.error('Upload error:', error);
          }
        );
    }
  }

  openEditModal(id: any, template: any,isEditing: boolean) {
    console.log('id', id)
    this.singleId = this.ChanelPartner.find((x: any) => x.userId == id);
    this.selectedRoleId = this.singleId?.roleId
    console.log('this.singleId', this.singleId.roleId);
    this.isEditing = isEditing;
    this.openModal(template ,isEditing);
    this.patchSignleValueTask(this.singleId);
  }

  editChannelPartnerByUserIds() {
    const zipCodeValue = this.channelForm.value.zipCode;
    const zipCodes = typeof zipCodeValue === 'string' ? zipCodeValue.split(',').map((code: string) => code.trim()) : [];
    const updatedprofileImage = this.uploadedImageUrl || this.singleId.profileImage;

    const sendData = {
      firstName: this.channelForm.value.firstName,
      lastName: this.channelForm.value.lastName,
      email: this.channelForm.value.email,
      mobileNo: this.channelForm.value.mobileNo,
      password: this.channelForm.value.password,
      profileImage : updatedprofileImage,
      zipCode: zipCodes // Use the split zip codes array
    };
  
    this.httpService.put(
      'editChannelPartnerByUserIds',
      this.singleId.userId,
       sendData
       ).subscribe(
      (res) => {
        this.clear();
        console.log('Updated successfully:', res);
        this.toastr.success('Updated successfully');
        this.channelForm.reset();
        this.getChanelPartnerProfileList();
        this.modalRef?.hide();
      },
      (error) => {
        console.log('Error updating Channel Partner:', error);
        this.toastr.error(error?.error?.messages.error);
        // const errorMessage = error?.messages?.error;
        // if (errorMessage) {
        //   this.toastr.error(errorMessage);
        // } else {
        //   this.toastr.error("An error occurred."); // Fallback message
        // }
      }
    );
  }
   
  patchSignleValueTask(singleId: any) {
    console.log('singleId', singleId)
    this.channelForm.get('roleId')?.setValue(this.singleId?.roleId);
    this.channelForm.get('firstName')?.patchValue(singleId.firstName);
    this.channelForm.get('lastName')?.patchValue(singleId.lastName);
    this.channelForm.get('email')?.patchValue(singleId.email);
    this.channelForm.get('mobileNo')?.patchValue(singleId.mobileNo);
    this.channelForm.get('password')?.patchValue(singleId.password);
    this.channelForm.get('password')?.patchValue(singleId.password);
    this.channelForm.get('zipCode')?.patchValue(singleId.zipCode.join(', '));
    // this.channelForm.get('roleId')?.patchValue(singleId.roleId);
    // this.channelForm.get('zipCode')?.patchValue(singleId.zipCode);
    this.profileImageSrc = singleId.profileImage;
  }
  submitForm() {
    if (this.channelForm.valid) {
      const formData = this.channelForm.value;
      console.log(formData);
    } else {
      // Handle form validation errors
    }
  }
 
  deleteChanelPartnerById(id: any) {
    Swal.fire({
      title: 'Delete',
      text: "Are you sure?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it'
    }).then((result) => {
      if (result.isConfirmed) {
        this.httpService.delete('deleteChanelPartnerById', id).subscribe(
          (res: any) => {
            console.log('res', res);
            this.getChanelPartnerProfileList();
          },
          (error: any) => {
            console.log('error', error);
          }
        );
      }
    });
  }

/*  deleteChanelPartnerById(id: any) {
    this.httpService.delete('deleteChanelPartnerById', id).subscribe(
      (res : any) => {
        console.log('res', res)
        this.getChanelPartnerProfileList();
      },
      (error : any) => {
        console.log('error', error)
      }
    ) 
  } */

  clear() {
    this.channelForm.reset();
    this.profileImageFile = null; // Clear the image URL
    this.profileImageSrc = null; // Clear the image file
    this.selectedRoleId = null; 
  }

  getData(): any[] {
    // Replace this with your actual data retrieval logic
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    return this.ChanelPartner?.slice(startIndex, endIndex);
  }

  get totalPages(): number {
    return Math.ceil(this.ChanelPartner?.length / this.itemsPerPage);
  }

  get pages(): number[] {
    return Array(this.totalPages)
      .fill(0)
      .map((_, i) => i + 1);
  }

  updateCurrentPage(page: number): void {
    if (page >= 1 && page <= this.totalPages) {
      this.currentPage = page;
    }
  }

  getPages(): number[] {
    const totalPages = this.getTotalPages();
    return Array(totalPages)
      .fill(0)
      .map((_, index) => index + 1);
  }

  getTotalPages(): number {
    return Math.ceil(this.ChanelPartner?.length / this.itemsPerPage);
  }

}
