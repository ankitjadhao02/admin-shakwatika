import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FarmersDetailsComponent } from './framersDetails/farmers-details/farmers-details.component';
import { ExpertProfileComponent } from './expertProfile/expert-profile/expert-profile.component';
import { ListExpertProfileComponent } from './expertProfile/list-expert-profile/list-expert-profile.component';
import { ListfarmersDetailsComponent } from './framersDetails/listfarmers-details/listfarmers-details.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { AddProductComponent } from './product/add-product/add-product.component';
import { ProductDetailsComponent } from './product/product-details/product-details.component';
import { UsersCardsComponent } from './user/users-cards/users-cards.component';
import { UsersEditComponent } from './user/users-edit/users-edit.component';
import { UsersProfileComponent } from './user/users-profile/users-profile.component';
import { RoleComponent } from './role/role.component';
import { CategoryComponent } from './category/category.component';
import { AddressComponent } from './address/address.component';
import { ComplaintComponent } from './complaint/complaint.component';
import { FeedBackComponent } from './feed-back/feed-back.component';
import { ChannelPartnerComponent } from './channel-partner/channel-partner.component';
import { OrderComponent } from './order/order.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';

var routingAnimation = localStorage.getItem('animate')

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: "product-list",
        component: ProductListComponent,
        data: {
          title: 'Products',
          breadcrumb: 'Product List',
          animation: [routingAnimation]
        }
      },
      {
        path: "add-product",
        component: AddProductComponent,
        data: {
          title: 'Add Product',
          breadcrumb: 'Add Product',
          animation: [routingAnimation]
        }
      },
      {
        path: "product-details",
        component: ProductDetailsComponent,
        data: {
          title: 'Product Details',
          breadcrumb: 'Product Details',
          animation: [routingAnimation]
        }
      },
      {
        path: 'users-cards',
        component: UsersCardsComponent,
        data: {
          title: 'Users Cards',
          breadcrumb: 'Users Cards',
          animation: [routingAnimation]
        }
      },
      {
        path: 'users-edit',
        component: UsersEditComponent,
        data: {
          title: 'Users',
          breadcrumb: 'Users Edit',
          animation: [routingAnimation]
        }
      },
      {
        path: 'users-profile',
        component: UsersProfileComponent,
        data: {
          title: 'Users Profile',
          breadcrumb: 'Users Profile',
          animation: [routingAnimation]
        }
      },
      {
        path: 'expert-Profile',
        component: ExpertProfileComponent,
        data: {
          title: 'Expert Profile',
          breadcrumb: 'Expert Profile',
          animation: [routingAnimation]
        }
      },
      {
        path: 'listExpertProfile',
        component: ListExpertProfileComponent,
        data: {
          title: 'Expert Profile',
          breadcrumb: 'Expert Profile List',
          animation: [routingAnimation]
        }
      },
      {
        path: 'farmersDetails',
        component: FarmersDetailsComponent,
        data: {
          title: 'Farmers Details',
          breadcrumb: 'Farmers Details',
          animation: [routingAnimation]
        }
      },
      {
        path: 'listfarmersDetails',
        component: ListfarmersDetailsComponent,
        data: {
          title: 'Farmers Details',
          breadcrumb: 'Farmers Details List',
          animation: [routingAnimation]
        }
      },
      {
        path: 'role',
        component: RoleComponent,
        data: {
          title: 'Role',
          breadcrumb: 'role',
          animation: [routingAnimation]
        }
      },
      {
        path: 'category',
        component: CategoryComponent,
        data: {
          title: 'Category',
          breadcrumb: 'category',
          animation: [routingAnimation]
        }
      },
      {
        path: 'address',
        component: AddressComponent,
        data: {
          title: 'Address',
          breadcrumb: 'address',
          animation: [routingAnimation]
        }
      },
      {
        path: 'complaint',
        component: ComplaintComponent,
        data: {
          title: 'Complaint',
          breadcrumb: 'complaint',
          animation: [routingAnimation]
        }
      },
      {
        path: 'feedBack',
        component: FeedBackComponent,
        data: {
          title: 'Feedback',
          breadcrumb: 'feedBack',
          animation: [routingAnimation]
        }
      },
      {
        path: 'channelPartner',
        component: ChannelPartnerComponent,
        data: {
          title: 'Channel Partner',
          breadcrumb: 'channelPartner',
          animation: [routingAnimation]
        }
      },
      {
        path: 'order',
        component: OrderComponent,
        data: {
          title: 'Order',
          breadcrumb: 'order',
          animation: [routingAnimation]
        }
      },
      {
        path: 'customer',
        component: CustomerDetailsComponent,
        data: {
          title: 'Customer Details',
          breadcrumb: 'customer',
          animation: [routingAnimation]
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationRoutingModule { }
