import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http/http.service';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {
  DashboardCountDetails: any;
  arrayCards:any[] = [];

  constructor(
    private httpService: HttpService,
  ) { 
    this.getDashboardCount();
    this.setCountData();
    this.DashboardCountDetails?.ProductCount
    console.log('this.DashboardCountDetails?.ProductCount', this.DashboardCountDetails?.ProductCount)
  }
  

  ngOnInit(): void {
  }

  setCountData() {
    this.arrayCards = [
      {
        label: 'Products',
        count: this.DashboardCountDetails?.ProductCount,
        url: "/configuration/product-list", content: "/configuration/product-list",
        // show: this.commonService.isRoleSuperAdmin,
      },
      {
        label: 'Category',
        count: this.DashboardCountDetails?.CategoryCount,
        url: "/configuration/category", content: "/configuration/category",
        // show: this.commonService.isRoleSuperAdmin,
      },
      {
        label: 'Farmer Details',
        count: this.DashboardCountDetails?.FarmerDetailsCount,
        url: "/configuration/listfarmersDetails", content: "/configuration/listfarmersDetails",
        // show: this.commonService.isRoleSuperAdmin,
      },
      {
        label: 'Expert Profiles',
        count: this.DashboardCountDetails?.ExpertProfileCount,
        url: "/configuration/listExpertProfile", content: "/configuration/listExpertProfile",
        // show: this.commonService.isRoleSuperAdmin,
      },
      {
        label: 'Address',
        count: this.DashboardCountDetails?.AddressCount,
        url: "/configuration/address", content: "/configuration/address",
        // show: this.commonService.isRoleSuperAdmin,
      },
      {
        label: 'Orders',
        count: this.DashboardCountDetails?.OrderCount,
        url: "/roles", content: "/roles",
        // show: this.commonService.isRoleSuperAdmin,
      },
      {
        label: 'Roles',
        count: this.DashboardCountDetails?.roleCount,
        url: "/configuration/role", content: "/configuration/role",
        // show: this.commonService.isRoleSuperAdmin,
      },
      {
        label: 'Users',
        count: this.DashboardCountDetails?.userCount,
        url: "/configuration/users-edit", content: "/configuration/users-edit",
        // show: this.commonService.isRoleSuperAdmin,
      },
    ];
  }

  getDashboardCount(){
    this.httpService.get('getDashboardCount').subscribe(
      (res) => {
        console.log('res', res)
        this.DashboardCountDetails = res;
        console.log('DashboardCountDetails', this.DashboardCountDetails)
        this.setCountData();
      },
      (error) => {
        console.log('error', error);
      }
    )
}

}
