import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from 'src/app/shared/services/http/http.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent {
  public validate = false;
  public tooltipValidation = false;
  productForm: FormGroup;
  productData: any;
  showSubmitButton = true;
  selectedId: any;


  constructor(private httpService: HttpService, private fb: FormBuilder) {
    this.productForm = this.fb.group({
      productType: ['', Validators.required],
      productName: ['', Validators.required],
      productQty: ['', Validators.required],
      productUnit: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.getProductList();
  }

  public submit() {
    this.validate = !this.validate;
    this.addProductFromData();
    this.productForm.reset();
    if (this.productForm.invalid) {
      return;
    }
  }

  getProductList() {
    this.httpService.get('product/getProductList').subscribe(
      (res) => {
        console.log('res', res)
        this.productData = res.data;
      },

      (error) => {
        console.log('error', error)
      }
    )
  }

  addProductFromData() {
    this.httpService.post('product/addProductFromData', this.productForm.value).subscribe(
      (res) => {
        console.log('res', res)
        this.getProductList();
        this.validate = !this.validate;
      },

      (error) => {
        console.log('error', error)
      }
    )
  }

  edit(item: any) {
    this.selectedId = item._id;
    this.productForm.patchValue({
      productType: item.productType,
      productName: item.productName,
      productQty: item.productQty,
      productUnit: item.productUnit,
    });
    this.showSubmitButton = false
  }

  updateProductList() {
    this.httpService.put('product/updateProduct', this.selectedId, this.productForm.value).subscribe(
      (res) => {
        console.log('res', res)
        this.getProductList();
        this.showSubmitButton = true;
        this.productForm.reset();
      },

      (error) => {
        console.log('error', error)
      }
    )
  }

  deleteProductList(i: any) {
    this.httpService.delete('product/deleteProductList', i).subscribe(
      (res: any) => {
        console.log('res', res)
        this.getProductList();
      },

      (error: any) => {
        console.log('error', error)
      }
    )
  }


}
