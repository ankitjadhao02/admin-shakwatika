import  { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CkEditorsComponent } from './ck-editors/ck-editors.component';
import { MdeEditorsComponent } from './mde-editors/mde-editors.component';

var routingAnimation = localStorage.getItem('animate')

 const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'ck-editors',
        component: CkEditorsComponent,
        data: {
          title: 'CK Editors',
          breadcrumb: 'CK Editors',
          animation: [routingAnimation]
        }
       
      },
      {
        path: 'mde-editors',
        component: MdeEditorsComponent,
        data: {
          title: 'Mde Editors',
          breadcrumb: 'Mde Editors',
          animation: [routingAnimation]
        }
       
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditorsRoutingModule { }
