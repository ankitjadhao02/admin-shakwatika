import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsLayoutRoutingModule } from './forms-layout-routing.module';
import { DefaultFormsComponent } from './default-forms/default-forms.component';
import { FormWizard1Component } from './form-wizard1/form-wizard1.component';
import { FormWizard2Component } from './form-wizard2/form-wizard2.component';
import { FormWizard3Component } from './form-wizard3/form-wizard3.component';
import { DefaultFormLayoutComponent } from './default-forms/default-form-layout/default-form-layout.component';
import { HorizontalFormLayoutComponent } from './default-forms/horizontal-form-layout/horizontal-form-layout.component';
import { InlineFormComponent } from './default-forms/inline-form/inline-form.component';
import { MegaFormComponent } from './default-forms/mega-form/mega-form.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ArchwizardModule } from 'angular-archwizard';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    DefaultFormsComponent,
    FormWizard1Component,
    FormWizard2Component,
    FormWizard3Component,
    DefaultFormLayoutComponent,
    HorizontalFormLayoutComponent,
    InlineFormComponent,
    MegaFormComponent
  ],
  imports: [
    CommonModule,
    FormsLayoutRoutingModule,
    SharedModule,
    ArchwizardModule,
    ReactiveFormsModule,
    NgbModule
  ]
})
export class FormsLayoutModule { }
