import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DefaultFormsComponent } from './default-forms/default-forms.component';
import { FormWizard1Component } from './form-wizard1/form-wizard1.component';
import { FormWizard2Component } from './form-wizard2/form-wizard2.component';
import { FormWizard3Component } from './form-wizard3/form-wizard3.component';

var routingAnimation = localStorage.getItem('animate')

 const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: "default-forms",
        component: DefaultFormsComponent,
        data: {
          title: 'Default Forms',
          breadcrumb: 'Default Forms',
          animation: [routingAnimation]
        }
      },
      {
        path: "form-wizard1",
        component: FormWizard1Component,
        data: {
          title: 'Form Wizard 1',
          breadcrumb: 'Form Wizard 1',
          animation: [routingAnimation]
        }
      },
      {
        path: "form-wizard2",
        component: FormWizard2Component,
        data: {
          title: 'Form Wizard 2',
          breadcrumb: 'Form Wizard 2',
          animation: [routingAnimation]
        }
        
      },
      {
        path: "form-wizard3",
        component: FormWizard3Component,
        data: {
          title: 'Form Wizard 3',
          breadcrumb: 'Form Wizard 3',
          animation: [routingAnimation]
        }
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsLayoutRoutingModule { }
