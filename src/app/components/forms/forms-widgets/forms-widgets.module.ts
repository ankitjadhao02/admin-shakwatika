import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsWidgetsRoutingModule } from './forms-widgets-routing.module';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { DatetimepickerComponent } from './datetimepicker/datetimepicker.component';
import { TouchspinComponent } from './touchspin/touchspin.component';
import { Select2Component } from './select2/select2.component';
import { SwitchComponent } from './switch/switch.component';
import { TypeaheadComponent } from './typeahead/typeahead.component';
import { ClipboardComponent } from './clipboard/clipboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxMatTimepickerModule } from 'ngx-mat-timepicker';
import { SharedModule } from 'src/app/shared/shared.module';
import { ColorVariantComponent } from './select2/color-variant/color-variant.component';
import { FullColorVariantComponent } from './select2/full-color-variant/full-color-variant.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { BasicColorComponent } from './switch/basic-color/basic-color.component';
import { BasicSwitchComponent } from './switch/basic-switch/basic-switch.component';
import { SwitchOutlineComponent } from './switch/switch-outline/switch-outline.component';
import { SwitchSizingComponent } from './switch/switch-sizing/switch-sizing.component';
import { SwitchUncheckedOutlineComponent } from './switch/switch-unchecked-outline/switch-unchecked-outline.component';
import { SwitchWithIconComponent } from './switch/switch-with-icon/switch-with-icon.component';
import { SwitchWithColorComponent } from './switch/switch-with-color/switch-with-color.component';
import { ClipboardModule } from 'ngx-clipboard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatepickerModule } from 'ng2-datepicker';

import { OwlDateTimeModule, OwlNativeDateTimeModule } from '@danielmoncada/angular-datetime-picker';

@NgModule({
  declarations: [
    DatepickerComponent,
    DatetimepickerComponent,
    TouchspinComponent,
    Select2Component,
    SwitchComponent,
    TypeaheadComponent,
    ClipboardComponent,
    ColorVariantComponent,
    FullColorVariantComponent,
    BasicColorComponent,
    BasicSwitchComponent,
    SwitchOutlineComponent,
    SwitchSizingComponent,
    SwitchUncheckedOutlineComponent,
    SwitchWithColorComponent,
    SwitchWithIconComponent 
  ],
  imports: [
    CommonModule,
    FormsWidgetsRoutingModule,
    FormsModule,
    NgbModule,
    NgxMatTimepickerModule,
    SharedModule,
    NgSelectModule,
    ClipboardModule,
    // BrowserAnimationsModule
    DatepickerModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ReactiveFormsModule
  ]
})
export class FormsWidgetsModule { }
