import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {

  constructor() {}

  // increment(id: any) {
  //   this.cartItem.filter((elem) =>
  //     elem.id === id ? this.counter++ : this.counter
  //   );
  // }

  // decrement(id: any) {
  //   if (this.counter > 0) {
  //     this.counter--;
  //   }
  // }

  ngOnInit(): void {}

  cartItem = [
    {
      id: 1,
      counter: 3,
      productImg: 'assets/images/product/1.png',
      productName: 'Long Top',
      price: 21,
      total: 63,
    },
    {
      id: 2,
      counter: 2,
      productImg: 'assets/images/product/13.png',
      productName: "Man's Jacket",
      price: 50,
      total: 100,
    },
    {
      id: 3,
      counter: 5,
      productImg: 'assets/images/product/4.png',
      productName: 'Women Skirt',
      price: 11,
      total: 55,
    },
  ];
}
